package com.simat.trakingonservice;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;

import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.location.LocationCallback;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.simat.trakingonservice.controller.ActivationController;
import com.simat.trakingonservice.controller.MainApplication;
import com.simat.trakingonservice.database.TrackingProvider;
import com.simat.trakingonservice.location.BestLocationListener;
import com.simat.trakingonservice.manager.BatteryManager;
import com.simat.trakingonservice.manager.Http2Manager;
import com.simat.trakingonservice.manager.HttpManager;
import com.simat.trakingonservice.manager.HttpManagerActivation;
import com.simat.trakingonservice.manager.LocationManager;
import com.simat.trakingonservice.manager.TCPManager;
import com.simat.trakingonservice.manager.TaskManager;
import com.simat.trakingonservice.model.ActivationModel;
import com.simat.trakingonservice.model.CTranModel;
import com.simat.trakingonservice.model.ConfigModel;
import com.simat.trakingonservice.model.DateTime;
import com.simat.trakingonservice.model.DeviceStatus;
import com.simat.trakingonservice.model.Friend;
import com.simat.trakingonservice.model.MTranModel;
import com.simat.trakingonservice.model.OfflineFileData;
import com.simat.trakingonservice.model.ProfileModel;
import com.simat.trakingonservice.model.ResultResponse;
import com.simat.trakingonservice.model.SmartPhone;
import com.simat.trakingonservice.model.UpdateDeviceLog;
import com.simat.trakingonservice.model.UpdateVersion;
import com.simat.trakingonservice.model.VersionModel;
import com.simat.trakingonservice.trakingonservice.TrackingUIActivity;
import com.simat.trakingonservice.util.ConfigInfo;
import com.simat.trakingonservice.util.ConfigInformation;
import com.simat.trakingonservice.util.Connectivity;
import com.simat.trakingonservice.util.DBHelper;
import com.simat.trakingonservice.util.DataInfo;
import com.simat.trakingonservice.util.DateFormatUtil;
import com.simat.trakingonservice.util.GetConfigPre;
import com.simat.trakingonservice.util.Utility;
import com.simat.trakingonservice.util.constantUtil;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.simat.trakingonservice.util.constanstURL.URL;
import static com.simat.trakingonservice.util.constanstURL.URLGETNUMBERAIS;

public class LocationService extends Service {

    private static final String TAG = "LocationService";

    private static final String CHANNEL_ID = "channel_01";
    private static final int NOTIFICATION_ID = 339922;

    private DateTime dateTime;


    String date = "";

    private final IBinder mBinder = new MyBinder();

    public CTranModel cTranModel;

    private Handler mainHandler = new Handler();
    private Runnable mainRunnable;

    private BatteryManager batteryManager;

    private boolean firstStart = true;
    private boolean isVehicleStop = true;

    private boolean isFirstNoGPS = true;
    private Location mLocation;

    private Timer batteryTimer = new Timer();

    private MainApplication trackingController;

    private ConfigInformation configurationInfo;

    private long sumDistant = 0;
    private int AlertCountSec = 0;
    private int countSec = 0;
    private int StopCountSec = 0;
    private long dist_m = 0;
    private String mDirection = "0";
    private String GStatus = "1";

    private String HandHeldStatus = "";
    private String START_SERVICE = "1";
    private String JobNo;

    private Location LocalBefore;
    private Location lastLocation;

    private boolean isGPSFix = false;

    private long mul_ac = 300;
    private List<Thread> TAlarm;

    private int runst1 = 5;

    private LocationManager localManager;

    private SimpleDateFormat smMin;
    private SimpleDateFormat smSec;

    private long lastTimeStamp;

    private float speed;

    private int TIME_COUNTER = 1;

    private boolean checkStatus = false;


    DBHelper mHelper;
    public static final int NOTIFICATION_ID_PROGRESS = 1512;


    private Timer timer = new Timer();
    SimpleDateFormat sdf;
    String currentDateandTime;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */



    @Override
    public void onCreate() {
        super.onCreate();

        TAlarm = new ArrayList<>();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
            startForeground(NOTIFICATION_ID, getNotification());
        } else {
            startForeground(1,new Notification());
        }


        trackingController = (MainApplication) getApplication();
        batteryManager = new BatteryManager();


        configurationInfo = new ConfigInformation();
        cTranModel = new CTranModel(getApplication());

       // initLocation();

        this.registerReceiver(mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenStateReceiver, screenStateFilter);

        IntentFilter gpsProvideFilter = new IntentFilter();
        gpsProvideFilter.addAction(android.location.LocationManager.PROVIDERS_CHANGED_ACTION);
        registerReceiver(mGPSProvideReceiver, gpsProvideFilter);
    }


    private Notification getNotification() {
        CharSequence text = "We are staking you";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
              .setContentText(text)
                .setContentTitle("Gps Tracker")
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        startTracking();

        trackingController.setMyService(this);
        smMin = new SimpleDateFormat("mm", Locale.getDefault());
        smSec = new SimpleDateFormat("ss", Locale.getDefault());

        init();
        checkLocationAvailable();
        ScheduleConfig();
        if (mainRunnable == null)
            ScheduleConfig();

        this.registerReceiver(mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        return START_STICKY;
    }

    private void startTracking() {
        Log.d(TAG, "startTracking");

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(30*1000); // milliseconds
        locationRequest.setFastestInterval(10*1000); // the fastest rate in milliseconds at which your app can handle location updates
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        try {
            LocationServices.getFusedLocationProviderClient(getApplicationContext()).requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    onLocationChanged(locationResult.getLastLocation());
                }
            }, Looper.myLooper());
        } catch (SecurityException se) {
            Log.e(TAG, se.getMessage());
            Log.e(TAG, "Go into settings and find Gps Tracker app and enable Location.");
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.e(TAG, "position: " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());


            if (location.getAccuracy() < 500.0f) {
                //stopLocationUpdates();

                //TODO: send locations
            }
        }
    }

    private void stopLocationUpdates() {
        Log.d(TAG, "Disconnect location update");

        stopSelf();
    }

    private void checkLocationAvailable() {

        if (localManager == null) {
            initLocation();
        } else {
            if (localManager.isGoogleApiClientConnecting()) {
                if (localManager.isGoogleApiClientConnected()) {
                    if (localManager.isLocationAvailable()) {
                        if (!localManager.isLocationUpdateStart()) {
                            localManager.startLocationUpdate();
                        }
                    } else {
                        localManager.setLocationUpdateStart(false);
                        Toast.makeText(getApplicationContext(), "Location not available", Toast.LENGTH_LONG).show();
                    }
                }

            } else {
                if (localManager.isGoogleApiClientConnected()) {
                    if (!localManager.isLocationUpdateStart()) {
                        localManager.startLocationUpdate();
                    }
                } else {
                    localManager.connect();
                }
            }
        }
    }

    private void PlayBatterySound() {

        batteryTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (batteryManager.getSoundCounter() > 0) {
                    MediaPlayer mp;
                    int count = batteryManager.getSoundCounter();
                    mp = MediaPlayer.create(LocationService.this, R.raw.battery);
                    mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    mAudioManager
                            .setStreamVolume(
                                    AudioManager.STREAM_MUSIC,
                                    mAudioManager
                                            .getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                                    0);

                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        public void onCompletion(MediaPlayer mp) {

                            mp.release();
                        }

                    });
                    mp.start();
                    count--;
                    batteryManager.setSoundCounter(count);
                }
            }
        }, 0, 2000);


    }

    private void PlaySpeedOverSound() {
        try {
            MediaPlayer mp2;
            mp2 = MediaPlayer.create(LocationService.this, R.raw.silent);
            mp2.setAudioStreamType(AudioManager.STREAM_MUSIC);
            AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mAudioManager
                    .setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager
                            .getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

            mp2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }

            });
            mp2.start();
            Thread.sleep(4000);
            // sound2play--;
        } catch (InterruptedException e) {

            e.printStackTrace();
        }

    }


    public void init() {
        PlayBatterySound();
        loadState();
        getSyncDateTime();
        getPODData();
    }

    private void ScheduleConfig() {
        mainRunnable = new Runnable() {
            @Override
            public void run() {

                Date mDate = new Date();
                String strMinute = smMin.format(mDate);
                String strSec = smSec.format(mDate);
                int intMinute = Integer.valueOf(strMinute);
                int intSec = Integer.valueOf(strSec);

                if (Connectivity.isConnected(LocationService.this)) {
                    if (ConfigInfo.getGoogleServiceSpeed()) {
                        CalculateDistance();

                    } else {
                        if (intSec % 3 == 0) {
                            Log.d("timerLoop", DateFormat.format("dd/mm/yy hh:mm:ss", new Date()).toString() + "  ==> 3 sec");

                            CalculateDistance();
                        }
                    }
                    if ((intMinute % 3 == 0) && (strSec.equals("00"))) {
                        Log.d("timerLoop", DateFormat.format("dd/mm/yy hh:mm:ss", new Date()).toString() + "  ==> 3 minute");
                        if (cTranModel.NotifyTrackingModel(LocationService.this).getU_success().equals("true")) {
                            if (Connectivity.isConnected(getApplication())) {
                                runAutoActivate();
                                UpdateVersion();
                                CheckDeviceStatus();
                                batteryManager.checkBattery();

                                Log.e("Min", "1111");

                            }
                        }
                    }
                    if ((intMinute % 5 == 0) && (strSec.equals("00"))) {
                        Log.d("timerLoop", DateFormat.format("dd/mm/yy hh:mm:ss", new Date()).toString() + "  ==> 5 minute");
                        if (cTranModel.NotifyTrackingModel(LocationService.this).getU_success().equals("true")) {
                            if (Connectivity.isConnected(getApplication())) {

                                Log.e("Long_far", "Long_far");
                                getPODData();
                                GetConfigTracking();
                                //readDataOffline();

                            }
                        }
                    }
                    if (strSec.equals("00")) {
                        Log.d("timerLoop", DateFormat.format("dd/mm/yy hh:mm:ss", new Date()).toString() + "  ==> 1 minute");

                        checkMemory();
                        checkLocationAvailable();
                        // batteryManager.checkBattery();
                    }
                    updateLED(1);
                } else {
                    updateLED(0);

                    if (intSec % 3 == 0) {
                        Log.d("timerLoop", DateFormat.format("dd/mm/yy hh:mm:ss", new Date()).toString() + "  ==> 3 sec");
                        CalculateDistance();

                    }
                }


                mainHandler.postDelayed(this, 1000);
            }
        };

        mainHandler.post(mainRunnable);
    }

    private void clearSetting() {
        DataInfo.clear();
        trackingController.setMyService(null);

    }

    private void getPODData() {
        ContentResolver cr = getContentResolver();

        Cursor cursor = cr.query(
                Uri.parse("content://com.simat.trackingprovider/tracks"), null,
                null, null, null);
        try {
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();

                    HandHeldStatus = cursor
                            .getString(cursor.getColumnIndex("U_StatusID"));
                    if (HandHeldStatus == null)
                        HandHeldStatus = "";

                    Log.d("HandHeldStatus", HandHeldStatus);

                    JobNo = cursor.getString(cursor.getColumnIndex("U_jobID"));

                    if (JobNo == null)
                        JobNo = "";

                }
                cursor.close();
            }
        } catch (Exception e) {
            Log.e("SQLError", e.toString());
        }


    }

    public void loadState() {
        try {
            SharedPreferences settings = PreferenceManager
                    .getDefaultSharedPreferences(this);
            dist_m = settings.getLong("distance_total", 0);
            mDirection = settings.getString("mDirection", "0");
            runst1 = settings.getInt("runst1", 0);
            // HandHeldStatus = settings.getString("HandHeldStatus", "");

            String LAT;
            String LNG;
            if (mLocation != null) {
                LAT = String.valueOf(mLocation.getLatitude());
                LNG = String.valueOf(mLocation.getLongitude());
            } else {
                CTranModel cTranModel = new CTranModel(getApplication());
                LAT = cTranModel.getTrackingModel().getU_lat();
                LNG = cTranModel.getTrackingModel().getU_lng();
            }

            float a = Float.valueOf(LAT);
            float b = Float.valueOf(LNG);
            Location locationA = new Location("point A");
            locationA.setLatitude(a);
            locationA.setLongitude(b);
            LocalBefore = locationA;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void saveState() {
        try {

            configurationInfo.setSpeedLimit(configurationInfo.getSpeedLimit());
            batteryManager.setBatteryLimit(batteryManager.getBatteryLimit());
            batteryManager.setBatteryLevel(batteryManager.getBatteryLevel());

            Log.e("getSpeedLimit", configurationInfo.getSpeedLimit() + "");

            // Write to Tracking DB
            ContentResolver cr = getContentResolver();

            ContentValues values = new ContentValues();
            if (mLocation != null) {
                values.put("U_lat", String.valueOf(mLocation.getLatitude()));
                values.put("U_lng", String.valueOf(mLocation.getLongitude()));
            }
            values.put("U_battery", batteryManager.getBatteryLevel());
            values.put("U_distance", (float) dist_m);

            Cursor c = cr.query(TrackingProvider.TRACKING_CONTENT_URI, null,
                    null, null, null);
            if (c != null) {
                if (c.getCount() == 0) {
                    cr.insert(TrackingProvider.TRACKING_CONTENT_URI, values);
                } else {
                    cr.update(TrackingProvider.TRACKING_CONTENT_URI, values,
                            null, null);
                }
            }
            if (c != null) {
                c.close();
            }
        } catch (Exception e) {
            Utility.AddLogcat("MainService.saveState", e.toString(), getApplicationContext());
            e.printStackTrace();
        }

    }

    private void CalculateDistance() {
        try {

            if (cTranModel.NotifyTrackingModel(LocationService.this).getU_success().equalsIgnoreCase("true")) {

                if ((!isGPSFix) || (!localManager.isLocationAvailable())) {
                    isGPSFix = false;
                }
                float dist_b = 0;

                if (isGPSFix) {
                    try {
                        getDistanceData();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (LocalBefore != null) {
                        dist_b = mLocation.distanceTo(LocalBefore);
                    } else {
                        LocalBefore = mLocation;
                    }

                    if (lastLocation == null)
                        lastLocation = mLocation;

                    float Speed = 0;

                    if (ConfigInfo.getGoogleServiceSpeed()) {
                        TIME_COUNTER = 3;
                        Speed = (float) (mLocation.getSpeed() * 3.6);
                        setSpeed(Speed);
                    } else {
                        TIME_COUNTER = 1;
                        long timeDelta = (mLocation.getTime() - lastTimeStamp) / 1000;

                        double distanceInMeters = mLocation.distanceTo(lastLocation);

                        //if (!mLocation.hasSpeed()) {

                        if (timeDelta > 0) {
                            Speed = (float) ((distanceInMeters / timeDelta) * 3.6);

                            Log.e("Calculations", "Distance: " + distanceInMeters + ", TimeDelta: " + timeDelta + " seconds"
                                    + ",speed: " + Speed
                                    + " LocationGetSpeed : " + mLocation.getSpeed() * 3.6
                                    + " Accuracy: " + mLocation.getAccuracy());
                        }
                    }

                    lastTimeStamp = mLocation.getTime();

                    if (Speed > configurationInfo.getSpeedLimit()) {
                        if (AlertCountSec >= configurationInfo.getAlarmDelayTime()) {

                            if ((AlertCountSec - configurationInfo.getAlarmDelayTime())
                                    % (5 * 60) == 0) {
                                String createDate = DateFormatUtil.parseDateToyyMMddkkmmss(getTimeSyncShort());
                                String dataSend = "*SM," + cTranModel.getTrackingModel().getU_compID() + ","
                                        + cTranModel.getTrackingModel().getU_HHID() + ","
                                        + new Utility().getDeviceId() + ","
                                        + "A101" + ","
                                        + "1000" + ","
                                        + createDate + ","
                                        + mLocation.getLatitude() + "," +
                                        +mLocation.getLongitude() + "," +
                                        +configurationInfo.getSpeedLimit() + "," +
                                        +Speed + ",##";
                                TCPManager.getInstance().send(GetConfigPre.getInstance().getSocket()
                                        , GetConfigPre.getInstance().getSocketPort()
                                        , dataSend, new TCPManager.SendCallback() {
                                            @Override
                                            public void onSuccess(String message) {
                                                Log.e("Websocket", "onSuccess >>>  " + "Speed");
                                            }

                                            @Override
                                            public void onFailed(String message) {
                                                Log.e("Websocket", "onFailed >>>  " + "Speed");
                                            }
                                        });
                            }
                            // SetAlarm Demo

                            Thread t = new Thread(new Runnable() {

                                @Override
                                public void run() {

                                    synchronized (getApplication()) {
                                        PlaySpeedOverSound();
                                    }
                                }
                            });
                            t.start();
                            TAlarm.add(t);

                            // sound2play = 1;
                            for (int i = 0; i < TAlarm.size(); i++) {

                                TAlarm.get(i).join();
                            }
                            // SetAlarm Demo
                        }
                        AlertCountSec++;

                    } else {
                        AlertCountSec = 0;
                    }

                    if (Speed > 20) {
                        SharedPreferences setCheck = getSharedPreferences(constantUtil.CHECKSCREEN, Context.MODE_PRIVATE);
                        boolean getcheck = setCheck.getBoolean(constantUtil.CHECKVALUE, true);

                        if (getcheck == true) {
                            if (checkStatus == false) {
                                AutoAppActive();
                                checkStatus = true;
                            } else {

                            }

                        }

                        if (runst1 == 20) {
                            setSTCallApp(runst1);

                            Log.e("checkOnscreen", "checkOnscreen -> " + getcheck);

                            if (getcheck) {
                                if (!(TaskManager.isPODRunningInForeground() || TaskManager.isTrackingRunningInForeground() || TaskManager.isMapRunningInForeground())) {
                                    Log.i("checkOnscreen", "isPod|Track|Map -> " + true);
                                    try {
                                        AutoAppActive();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }

                        }
                        if (runst1 < 6)
                            runst1++;
                    }
                    if (Speed != 0) {
                        if (Speed <= 30) {
                            if (((dist_b) > 50)
                                    && (dist_b > mLocation
                                    .getAccuracy())) {

                                if (dist_b < (mLocation
                                        .getAccuracy() * mul_ac)) {
                                    float dir = LocalBefore
                                            .bearingTo(mLocation);
                                    if (dir < 0) {
                                        dir = dir + 360;
                                    }
                                    mDirection = "" + (int) dir;


                                    dist_m += (long) dist_b;

                                    sumDistant += (long) dist_b;

                                    GStatus = "2";
                                    Log.e("Websocket", "Moving");
                                    setSpeed(Speed);
                                    DataSent();
                                    LocalBefore = mLocation;
                                    isVehicleStop = true;
                                    mul_ac = 100;
                                } else {
                                    mul_ac += mul_ac;
                                }
                            }

                        } else {

                            if (((dist_b) > (Speed * 3))
                                    && (dist_b > mLocation
                                    .getAccuracy())) {
                                if (dist_b < (mLocation
                                        .getAccuracy() * mul_ac)) {
                                    float dir = LocalBefore
                                            .bearingTo(mLocation);
                                    if (dir < 0) {
                                        dir = dir + 360;
                                    }
                                    mDirection = "" + (int) dir;

                                    dist_m += (long) dist_b;
                                    sumDistant += (long) dist_b;

                                    GStatus = "2";
                                    //Gps_speed = speedb;

                                    Log.d("Websocket", "Moving speed*3");
                                    setSpeed(Speed);
                                    DataSent();
                                    isVehicleStop = true;

                                    mul_ac = 100;
                                    LocalBefore = mLocation;
                                } else {
                                    mul_ac += mul_ac;
                                }
                            }
                        }

                    } else {
                        if ((isVehicleStop)
                                || ((dist_b > 50) && (dist_b > mLocation
                                .getAccuracy()))) {
                            if (StopCountSec > 4) {

                                if (firstStart) {
                                    firstStart = false;
                                }
                                LocalBefore = mLocation;
                                //DateStatusChange = getTimeSync();
                                runst1 = 0;

                                GStatus = "3";
                                StopCountSec = 0;
                                Log.d("Websocket", "Offline");
                                setSpeed(Speed);
                                DataSent();
                                isVehicleStop = false;


                            }
                        } else {

                            // if (StopCountSec >= 60 * 3) {
                            if (StopCountSec >= 60 * TIME_COUNTER) {

                                GStatus = "3";
                                StopCountSec = 0;
                                //Gps_speed = 0;
                                Log.d("Websocket", "Stop");
                                setSpeed(Speed);
                                DataSent();

                            }
                        }
                        StopCountSec++;

                    }
                    countSec = 0;
                    isFirstNoGPS = true;

                } else {

                    GStatus = "1";

                    countSec++;
                    if ((countSec >= 60 * TIME_COUNTER) || isFirstNoGPS) {
                        //if ((countSec >= 60 * 3) || isFirstNoGPS) {
                        countSec = 0;
                        //Gps_speed = 0;
                        Log.e("Websocket", "No GPS");
                        DataSent();
                    }
                    isFirstNoGPS = false;
                }
            } else {
                updateLED(0);
            }
        } catch (Exception e) {
            Log.e("Websocket", "Error before send : " + e.toString());

        }
        lastLocation = mLocation;
    }

    public void DataSent() {

        try {

            saveState();
            SendData();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    int ID = -1;

    private void insertData(String jsonOffile) {

        Friend friend = new Friend();
        friend.setFirstName(jsonOffile);
        if (ID == -1) {
            mHelper.addFriend(friend);
        }
    }



    private void SendData() throws IOException {

        //createNotification();

        double longitude = 0;
        double latitude = 0;
        double Accuracy = 0;
        double Speed = 0;
        int Heading;
        //  String Distance = ("" + dist_b);
        String CreateOn;
        final MTranModel mTranModel = new MTranModel();
        MTranModel.GPS gps = new MTranModel.GPS();
        MTranModel.Device device = new MTranModel.Device();
        MTranModel.Config config = new MTranModel.Config();


        if ((mLocation != null) && (!GStatus.equalsIgnoreCase("1"))) {

            if (GStatus.equalsIgnoreCase("2") || GStatus.equalsIgnoreCase("3")) {
                longitude = mLocation.getLongitude();
                latitude = mLocation.getLatitude();
                Accuracy = mLocation.getAccuracy();
                //Speed = mLocation.getSpeed() * 3.6f;
                Speed = getSpeed();
            }
        } else {
            CTranModel cTranModel = new CTranModel(getApplication());
            latitude = Double.valueOf(cTranModel.getTrackingModel().getU_lat());
            longitude = Double.valueOf(cTranModel.getTrackingModel().getU_lng());
        }

        Heading = Integer.valueOf(mDirection);
        CreateOn = getTimeSync();
        String actionApp = "1";
        String distance = "" + Math.round(dist_m * 100.0) / 100.0;
        String CStatus = "2";

        mTranModel.setHHID(cTranModel.getTrackingModel().getU_HHID());
        mTranModel.setStatusUpdate(getTimeSync());
        mTranModel.setCreateDate(CreateOn);
        mTranModel.setCustomerID(cTranModel.getTrackingModel().getU_compID());
        mTranModel.setIsStart(Integer.parseInt(START_SERVICE));
        mTranModel.setJobNo(JobNo);
        mTranModel.setDataType("OFFLINE");


        gps.setBattery(batteryManager.getBatteryLevel());
        gps.setLat(latitude);
        gps.setLng(longitude);
        gps.setSpeed(Speed);
        gps.setHeading(Heading);
        gps.setDistance(Double.parseDouble(distance));
        gps.setAccuracy((int) Accuracy);
        if (mLocation != null) {
            gps.setDate(mLocation.getTime());
        } else {
            gps.setDate(0);
        }

        gps.setStatus(GStatus);

        device.setAction(actionApp);
        device.setEngine(CStatus);
        device.setStatus(HandHeldStatus);

        config.setBattery(batteryManager.getBatteryLimit());
        config.setSpeed(configurationInfo.getSpeedLimit());

        mTranModel.setGPS(gps);
        mTranModel.setDevice(device);
        mTranModel.setConfig(config);


        SharedPreferences sp = getSharedPreferences(DataInfo.PREF_STATE, Context.MODE_PRIVATE);
        boolean isMaster = sp.getBoolean("isMaster", false);
        Log.e("Websocket", "check is master : " + isMaster);
        if (isMaster) {

            if (Connectivity.isConnected(getApplication())) {
                String createDate = DateFormatUtil.parseDateToyyMMddkkmmss(CreateOn);
                String statusUpdate = DateFormatUtil.parseDateToyyMMddkkmmss(getTimeSync());
                final String ssend = "*SM," + mTranModel.getCustomerID() + ","
                        + mTranModel.getHHID() + ","
                        + new Utility().getDeviceId() + ","
                        + "A100" + "," //Command 1000 = GPS
                        + createDate + ","
                        + device.getStatus() + ","
                        + gps.getLat() + ","
                        + gps.getLng() + ","
                        + gps.getSpeed() + ","
                        + gps.getBattery() + ","
                        + gps.getHeading() + ","
                        + statusUpdate + ","
                        + gps.getStatus() + ","
                        + device.getEngine() + ","
                        + gps.getDistance() + ","
                        + gps.getAccuracy() + ","
                        + "1" + ","
                        + "##";

                Log.e("ssend", ssend);

                TCPManager.getInstance().send(GetConfigPre.getInstance().getSocket()
                        , GetConfigPre.getInstance().getSocketPort()
                        , ssend, new TCPManager.SendCallback() {
                            @Override
                            public void onSuccess(String message) {
                                Log.e("Websocket", "onSuccess >>>  " + "Speed");
                            }

                            @Override
                            public void onFailed(String message) {
                                Log.e("Websocket", "onFailed >>>  " + "Speed");
                            }
                        });

            } else {
                synchronized (getApplication()) {
                    String createDate = DateFormatUtil.parseDateToyyMMddkkmmss(CreateOn);
                    String statusUpdate = DateFormatUtil.parseDateToyyMMddkkmmss(getTimeSync());

                    final String ssendOffline = "*SM," + mTranModel.getCustomerID() + ","
                            + mTranModel.getHHID() + ","
                            + new Utility().getDeviceId() + ","
                            + "A100" + "," //Command 1000 = GPS
                            + createDate + ","
                            + device.getStatus() + ","
                            + gps.getLat() + ","
                            + gps.getLng() + ","
                            + gps.getSpeed() + ","
                            + gps.getBattery() + ","
                            + gps.getHeading() + ","
                            + statusUpdate + ","
                            + gps.getStatus() + ","
                            + device.getEngine() + ","
                            + gps.getDistance() + ","
                            + gps.getAccuracy() + ","
                            + "0" + ","
                            + "##";

                    mHelper = new DBHelper(getApplicationContext());
                    writeMTranOffline(mTranModel, ssendOffline);
                    Log.e("ssendOfflinr", ssendOffline);


                    updateLED(0);
                }
            }
        } else {
            updateLED(0);
        }

        START_SERVICE = "0";

    }


    private void updateLED(int status) {
        Intent intent = new Intent(TrackingUIActivity.LED_RECEIVER);
        intent.putExtra("status", status);
        sendBroadcast(intent);
    }

    private void getDistanceData() throws Exception {
        ContentResolver cr = getContentResolver();

        Cursor cursor = cr.query(
                Uri.parse("content://com.simat.trackingprovider/tracks"), null,
                null, null, null);
        if (cursor != null) {
            if (cursor.getCount() != 0) {
                cursor.moveToFirst();

                dist_m = (long) cursor.getFloat(cursor
                        .getColumnIndex("U_distance"));

            }
            cursor.close();
        }
    }

    public void AutoAppActive() {
        Log.d("Checkallstatus", "autoapp work");
        Thread thread = new Thread() {
            @Override
            public void run() {

                Intent dialogIntent = new Intent(getBaseContext(),
                        TrackingUIActivity.class);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // FLAG_ACTIVITY_NEW_TASK
                getApplication().startActivity(dialogIntent);
            }
        };

        thread.start();
    }

    //region Read & Write & Send Data
    private void writeMTranOffline(MTranModel mTranOffline, String jsonOnline) {

        Log.e("jsonOnline", jsonOnline);
        insertData(jsonOnline);
    }

    //endregion

    //region UpdateVersion
    private void UpdateVersion() {
        String current = DataInfo.getVersion();
        String versionName = new Utility().getVersion();
        Log.d("httpConnect", "Old Version : " + current);
        Log.d("httpConnect", "Current Version : " + versionName);

        if (!current.equals(versionName)) {
            Log.d("httpConnect", "onUpdateVersion");
            VersionModel vm = new VersionModel();
            vm.setApplicationName("gps");
            vm.setVersionName(new Utility().getVersion());
            Call<UpdateVersion> callVersion =
                    HttpManager.getInstance().getService().UpdateVersion(vm);

            callVersion.enqueue(new Callback<UpdateVersion>() {
                @Override
                public void onResponse(Call<UpdateVersion> call, Response<UpdateVersion> response) {
                    if (response.isSuccessful()) {
                        UpdateVersion versionCollection = response.body();
                        if (versionCollection.isSuccess()) {

                            String versionName = new Utility().getVersion();
                            Log.d("httpConnect", "UpdateVersion");

                            DataInfo.setVersion(versionName);


                        } else {
                            Log.d("versionCheck", "" + versionCollection.getMessage());
                        }

                    } else {
                        try {
                            Log.e("versionCheck", "Updateversion : " + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UpdateVersion> call, Throwable t) {
                    Log.e("versionCheck", "Updateversion : " + t.toString());
                }
            });
        } else {
            Log.d("httpConnect", "Skip");
        }

    }
    //endregion

    //region CheckDeviceStatus
    private void CheckDeviceStatus() {
        Log.e("CheckDeviceStatus", "onCheckStatus");
        Call<DeviceStatus> callProfile = HttpManager.getInstance().getService().DeviceStatus();
        callProfile.enqueue(
                new Callback<DeviceStatus>() {
                    @Override
                    public void onResponse(Call<DeviceStatus> call, Response<DeviceStatus> response) {
                        ActivationController ac = new ActivationController();
                        if (response.isSuccessful()) {
                            DeviceStatus deviceStatus = response.body();

                            if (deviceStatus.isSuccess()) {
                                DeviceStatus.Datas datas = deviceStatus.getDatas();
                                if (!datas.isStatus()) {

                                    Log.e("CheckDeviceStatus_1", "CheckDeviceStatus : " + " Inactive");
                                    ac.UnActivate();

                                } else {
                                    DataInfo.setShortName(datas.getShortName());
                                    ConfigInfo.setMaster(datas.isIsMaster());
                                    ConfigInfo.setLog(datas.isLog());
                                }

                            } else {
                                switch (deviceStatus.getRefCode()) {

                                    //Case False//False Message
                                    //"00004", "License was not found."
                                    //"00007", "Device was not found."

                                    case "00004":
                                    case "00007":
                                        Log.e("inactived", "Error 00007 : " + " Inactive");
                                        ac.UnActivate();

                                        break;
                                }
                                Log.e("CheckDeviceStatus", deviceStatus.getRefCode() + " : " + deviceStatus.getMessage());
                            }
                        } else

                        {
                            try {
                                Log.e("CheckDeviceStatus", "CheckDeviceStatus : " + response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<DeviceStatus> call, Throwable t) {
                        Log.e("CheckDeviceStatus", "CheckDeviceStatus : " + t.toString());

                    }
                }
        );
    }
    //endregion

    //region GetConfigTracking
    public void GetConfigTracking() {
        Log.e("httpConnect", "--->onGetConfigTracking");
        Call<ConfigModel> callconfig = HttpManager.getInstance().getService().Config("");
        callconfig.enqueue(new Callback<ConfigModel>() {
            @Override
            public void onResponse(Call<ConfigModel> call, Response<ConfigModel> response) {
                if (response.isSuccessful()) {
                    ConfigModel configCollection = response.body();

                    if (configCollection.isSuccess()) {
                        if (configCollection.getDatas() != null) {
                            ConfigModel.Datas vDatasEntity = configCollection.getDatas();
                            Log.e("httpConnect", new Gson().toJson(vDatasEntity));
                            batteryManager.setBatteryLimit(vDatasEntity.getBatteryLowLimit());
                            configurationInfo.setSpeedLimit(vDatasEntity.getSpeedLimit());

                            configurationInfo.setAlarmDelayTime(vDatasEntity.getAlarmDelayTime());

                            Location locationA = new Location("point A");
                            locationA.setLatitude(vDatasEntity.getLocation().getLat());
                            locationA.setLongitude(vDatasEntity.getLocation().getLng());
                            LocalBefore = locationA;

                            ConfigInfo.setKeyUpdateLast(configCollection.getDatas().getUpdateLast());
                            ConfigInfo.setKeyConfig(configCollection.getDatas().getRefCode());

                            Log.e("configCollection", configCollection.getDatas().getRefCode());


                        } else {
                            Log.d("httpConnect", "GetConfigTracking : Not Change");
                        }
                    }

                } else {
                    try {
                        Log.e("httpConnect_error", "GetConfigTracking : " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("httpConnect_error", "GetConfigTracking : " + e.getMessage());
                        Log.e("httpConnect_error", "GetConfigTracking : " + e.getLocalizedMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ConfigModel> call, Throwable t) {
                Log.e("onFailure", "GetConfigTracking : " + t.toString());
            }
        });
    }

    //region runAutoActivate

    public void runAutoActivate() {

        String DeviceNumber;
        CTranModel cTranModel = new CTranModel(getApplication());
        // Log.d("httpConnect", "AutoActivate : " + cTranModel.getTrackingModel().getU_success());
        if (!cTranModel.getTrackingModel().getU_success()
                .equalsIgnoreCase("true")) {
            DeviceNumber = getAutoNumberAIS();
            if (DeviceNumber.isEmpty()) {
                return;
            }

            final ActivationModel activationModel = new ActivationModel();
            activationModel.setActivate(true);
            activationModel.setHHID(DeviceNumber);
            activationModel.setUUID(DataInfo.getUUID());

            Call<ProfileModel> call = new HttpManagerActivation().getService().Activation(activationModel);
            try {

                call.enqueue(new Callback<ProfileModel>() {
                    @Override
                    public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {

                        if (response.isSuccessful()) {
                            ProfileModel profileModel = response.body();

                            ActivationController ac = new ActivationController();
                            if (profileModel.isSuccess()) {

                                if (profileModel.getDatas() != null) {


                                    profileModel.getDatas().setHHID(activationModel.getHHID());
                                    profileModel.getDatas().setUUID(activationModel.getUUID());
                                    ac.setProfileModel(profileModel);
                                    ac.Activate();

                                    setDist(0);
                                    setDisPlayDistance(0);
                                    Log.d("Websocket", "AutoActive");
                                    DataSent();

                                    Log.d("httpConnect", "" + profileModel.getMessage());
                                }


                            } else {
                                Log.d("inactived", "AutoActivate : " + " Inactive");
                                ac.UnActivate();


                                Log.d("httpConnect", "AutoActivate : " + profileModel.getMessage());

                            }

                        } else {
                            try {
                                Log.d("httpConnect", "AutoActivate : " + response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfileModel> call, Throwable t) {
                        Log.d("onFailure", "AutoActivate : " + t.toString());
                    }
                });

            } catch (Exception e) {
                Log.d("httpConnect", "AutoActivate : " + e.toString());
            }
        }
    }

    //endregion
    public void setDist(long dist) {
        dist_m = dist;
        SharedPreferences settings1 = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings1.edit();
        editor.putLong("distM", dist_m);
        editor.apply();
    }

    public void setSTCallApp(int st) {
        runst1 = st;
        SharedPreferences settings1 = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings1.edit();
        editor.putInt("runst1", runst1);
        editor.apply();

    }

    public long getDisPlayDistance() {
        return sumDistant;
    }

    public void setDisPlayDistance(long d) {
        sumDistant = d;
        SharedPreferences settings1 = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings1.edit();
        editor.putLong("sumDistant", sumDistant);
        editor.apply();
    }

    public void getSyncDateTime() {

        Thread networkThread = new Thread() {
            @Override
            public void run() {

                if (Connectivity.isConnected(LocationService.this)) {
                    boolean getpass = false;
                    String SOAP_ACTION = "http://www.skyfrog.net/ExGPS_SyncDateTime";
                    String METHOD_NAME = "ExGPS_SyncDateTime";
                    while (!getpass) {
                        try {
                            CTranModel cTranModel = new CTranModel(getApplication());
                            SoapObject request = new SoapObject(
                                    constantUtil.NAMESPACE, METHOD_NAME);
                            request.addProperty("compID", cTranModel
                                    .getTrackingModel().getU_compID());
                            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                                    SoapEnvelope.VER11);
                            envelope.dotNet = true;
                            envelope.setOutputSoapObject(request);


                            HttpTransportSE ht = new HttpTransportSE(URL);
                            ht.call(SOAP_ACTION, envelope);

                            final SoapPrimitive response = (SoapPrimitive) envelope
                                    .getResponse();

                            final String str = response.toString();

                            System.out.println("Timeout reached!!! " + str);

                            try {
                                String sfind = "U_Message";
                                int indf = str.indexOf("<" + sfind + ">");
                                int indl = str.indexOf("</" + sfind + ">");
                                int intc = sfind.length() + 2;
                                String datetimesyc1 = str.substring(indf + intc,
                                        indl);

                                int t_year;
                                int t_month;
                                int t_day;
                                int t_hour;
                                int t_min;
                                int t_sec;

                                String mn = datetimesyc1.substring(0, 4);
                                t_year = Integer.parseInt(mn);
                                mn = datetimesyc1.substring(4, 6);
                                t_month = Integer.parseInt(mn);
                                mn = datetimesyc1.substring(6, 8);
                                t_day = Integer.parseInt(mn);
                                mn = datetimesyc1.substring(8, 10);
                                t_hour = Integer.parseInt(mn);
                                mn = datetimesyc1.substring(10, 12);
                                t_min = Integer.parseInt(mn);
                                mn = datetimesyc1.substring(12, 14);
                                t_sec = Integer.parseInt(mn);

                                if (dateTime == null) {
                                    dateTime = new DateTime(t_year, t_month - 1,
                                            t_day, t_hour, t_min, t_sec);

                                    Log.d("Websocket", "SynDateTime");
                                    DataSent();
                                }

                                getpass = true;


                            } catch (Exception e) {
                                Utility.AddLogcat("MainService.getinitdata3", e.toString(), getApplicationContext());
                                e.printStackTrace();

                            }

                        } catch (Exception e) {
                            Utility.AddLogcat("MainService.getinitdata3", e.toString(), getApplicationContext());
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        networkThread.start();
    }

    public String getTimeSyncShort() {
        String dateTimeSync;

        dateTimeSync = (String) DateFormat.format("yyyyMMddkk",
                new Date());

        return dateTimeSync;
    }

    public String getTimeSync() {
        String dateTimeSyn;
        if (this.dateTime != null) {
            dateTimeSyn = dateTime.getDate();

        } else {

            dateTimeSyn = (String) DateFormat.format("yyyy-MM-dd kk:mm:ss",
                    new Date());

        }
        return dateTimeSyn;
    }

    public String getAutoNumberAIS() {
        String MobileNumber = "";
        if (!Connectivity.isConnected(this)) {
            return MobileNumber;
        }
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL url = new URL(URLGETNUMBERAIS);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(10000);

            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));

                String line;

                while ((line = reader.readLine()) != null) {
                    if (line.contains("username")) {

                        boolean chknum = Utility.isNumberFormat(line.substring(
                                (line.indexOf("username")) + 9,
                                (line.indexOf("username")) + 20));
                        if (chknum) {
                            MobileNumber = line.substring(
                                    (line.indexOf("username")) + 9,
                                    (line.indexOf("username")) + 20);
                            MobileNumber = "0" + MobileNumber.substring(2, 11);
                            return MobileNumber;
                        }

                    }

                }

            } catch (IOException e) {
                Utility.AddLogcat("MainService.readStream", e.toString(), getApplicationContext());
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                Utility.AddLogcat("MainService.readStream", e.toString(), getApplicationContext());

            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return MobileNumber;

    }


    private void initLocation() {

        localManager = new LocationManager(getApplicationContext());

        LocationRequest request = new LocationRequest();
        request.setFastestInterval(2000).setInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL)
                .setSmallestDisplacement(MIN_DISTANCE_CHANGE_FOR_UPDATES);

        localManager
                .setBestLocationListener(locationListener)
                .setLocationRequest(request)
                .build();


        if (mLocation != null) {
            LocalBefore = mLocation;
        }

    }

    public void checkMemory() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        long availableMegs = mi.availMem / 1048576L;

//Percentage can be calculated for API 16+
        long percentAvail = mi.availMem / mi.totalMem;
        long total = mi.totalMem / 1048576L;
        long Useage = total - availableMegs;
        Log.d("Chkmem", "--------------------------------------");
        Log.d("Chkmem", "Useage: " + Useage + " MB");
        Log.d("Chkmem", "AvailableMegs: " + availableMegs + " MB");
        Log.d("Chkmem", "Total: " + total + " MB");
        Log.d("Chkmem", "Percent: " + percentAvail + " %");
        Log.d("Chkmem", "--------------------------------------");
                /*
                1024 bytes      == 1 kilobyte
                1024 kilobytes  == 1 megabyte
                1024 * 1024     == 1048576
                 */

    }

    private void StopService() {
        clearSetting();
        mainHandler.removeCallbacks(mainRunnable);
        localManager.disconnect();
        unregisterReceiver(mBatInfoReceiver);
        unregisterReceiver(mScreenStateReceiver);
        unregisterReceiver(mGPSProvideReceiver);
        updateLED(0);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        StopService();
        startService(rootIntent);
    }


    private BestLocationListener locationListener = new BestLocationListener() {
        @Override
        public void onLocationUpdate(Location location, boolean isFresh) {

            float hasAccuracy = location.getAccuracy();
            if (hasAccuracy > 100) {

                isGPSFix = false;

            } else {

                Log.d("loclistener",
                        "Type :"
                                + " Lat :"
                                + location.getLatitude()
                                + " Long:"
                                + location.getLongitude()
                                + " Accurancy:"
                                + location.getAccuracy()
                                + " Speed "
                                + String.format("%.2f",
                                location.getSpeed() * 3.6)
                                + " Date " + location.getTime()
                                + " provider :" + location.getProvider());

                if (LocalBefore == null) {
                    LocalBefore = location;
                }
                if (lastLocation == null) {
                    lastLocation = location;
                }

                isGPSFix = true;
                mLocation = location;

            }
        }


    };

    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int level = intent.getIntExtra(android.os.BatteryManager.EXTRA_LEVEL, 0);
            int plug = intent.getIntExtra(android.os.BatteryManager.EXTRA_PLUGGED, -1);


            if (plug != 0) {
                level = level * -1;

                batteryManager.setSoundCounter(0);
                batteryManager.setPluge(true);
            } else {
                batteryManager.setPluge(false);
            }

            batteryManager.setBatteryLevel(level);
        }
    };

    private BroadcastReceiver mScreenStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {

                DataInfo.setScreenStatus("On");
                Log.d("ScreenAction", Intent.ACTION_SCREEN_ON);

            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

                DataInfo.setScreenStatus("Off");

                Log.d("ScreenAction", Intent.ACTION_SCREEN_OFF);
            }


            SmartPhone sm = new SmartPhone();
            if (date != null) {
                sm.setDate(date);
            } else {
                sm.setDate(date);
            }

            sm.setType("screen");
            sm.setValue(DataInfo.getScreenStatus());
            List<SmartPhone> phones = new ArrayList<>();
            phones.add(sm);
            Call<ResultResponse> call = new Http2Manager().getService().SmartPhone(phones);
            call.enqueue(new Callback<ResultResponse>() {
                @Override
                public void onResponse(Call<ResultResponse> call, Response<ResultResponse> response) {
                    if (response.isSuccessful()) {
                        ResultResponse res = response.body();
                        if (res.isSuccess()) {
                            Log.e("SmartPhoneHTTP", res.getMessage());

                        } else {
                            Log.e("SmartPhoneHTTP", res.getMessage());
                        }
                    } else {
                        Log.d("SmartPhoneHTTP", "Code : " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<ResultResponse> call, Throwable t) {

                    Log.e("SmartPhoneHTTP", t.toString());
                    Log.e("SmartPhoneHTTP", t.getMessage());
                    Log.e("SmartPhoneHTTP", t.getLocalizedMessage());
                }
            });

        }
    };

    private BroadcastReceiver mGPSProvideReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(android.location.LocationManager.PROVIDERS_CHANGED_ACTION)) {
                DataInfo.setGPSProvide(Connectivity.getLocationMode());

                Log.d("ScreenAction", Connectivity.getLocationMode());

                SmartPhone sm = new SmartPhone();
                sm.setDate(date);
                sm.setType("gps");
                sm.setValue(DataInfo.getGPSProvide());
                List<SmartPhone> phones = new ArrayList<>();
                phones.add(sm);
                Call<ResultResponse> call = new Http2Manager().getService().SmartPhone(phones);
                call.enqueue(new Callback<ResultResponse>() {
                    @Override
                    public void onResponse(Call<ResultResponse> call, Response<ResultResponse> response) {
                        if (response.isSuccessful()) {
                            ResultResponse res = response.body();
                            if (res.isSuccess()) {
                                Log.d("SmartPhoneHTTP", res.getMessage());

                            } else {
                                Log.d("SmartPhoneHTTP", res.getMessage());
                            }
                        } else {
                            Log.d("SmartPhoneHTTP", "Code : " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResultResponse> call, Throwable t) {

                        Log.d("SmartPhoneHTTP", t.toString());
                    }
                });

            }
        }
    };

    
    public class MyBinder extends Binder {
        LocationService getService() {
            return LocationService.this;
        }
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

}