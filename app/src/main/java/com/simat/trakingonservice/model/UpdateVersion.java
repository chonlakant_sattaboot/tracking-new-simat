package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 25/3/2559.
 */
public class UpdateVersion extends ResultWithModel {


    private Datas datas;


    public Datas getDatas() {
        return datas;
    }

    public void setDatas(Datas datas) {
        this.datas = datas;
    }

    public static class Datas {
        private String HHID;
        private String UUID;
        private String GPS;
        private String POD;

        public String getHHID() {
            return HHID;
        }

        public void setHHID(String HHID) {
            this.HHID = HHID;
        }

        public String getUUID() {
            return UUID;
        }

        public void setUUID(String UUID) {
            this.UUID = UUID;
        }

        public String getGPS() {
            return GPS;
        }

        public void setGPS(String GPS) {
            this.GPS = GPS;
        }

        public String getPOD() {
            return POD;
        }

        public void setPOD(String POD) {
            this.POD = POD;
        }
    }
}
