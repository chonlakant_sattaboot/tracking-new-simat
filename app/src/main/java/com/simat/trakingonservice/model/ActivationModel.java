package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 12/21/2015.
 */
public class ActivationModel {

    String HHID;
    String UUID;
    String UDID;
    boolean Activate;

    public ActivationModel() {
    }

    public String getHHID() {
        return HHID;
    }

    public void setHHID(String HHID) {
        this.HHID = HHID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public boolean isActivate() {
        return Activate;
    }

    public void setActivate(boolean activate) {
        Activate = activate;
    }

    public String getUDID() {
        return UDID;
    }

    public void setUDID(String UDID) {
        this.UDID = UDID;
    }
}
