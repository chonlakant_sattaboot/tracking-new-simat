package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 11/1/2559.
 */
public class VersionModel {

    /**
     * VersionName : 1.6
     * ApplicationName : gps
     */

    private String VersionName;
    private String ApplicationName;

    public void setVersionName(String VersionName) {
        this.VersionName = VersionName;
    }

    public void setApplicationName(String ApplicationName) {
        this.ApplicationName = ApplicationName;
    }

    public String getVersionName() {
        return VersionName;
    }

    public String getApplicationName() {
        return ApplicationName;
    }
}

