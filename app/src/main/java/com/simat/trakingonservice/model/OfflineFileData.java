package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 6/15/2016.
 */
public class OfflineFileData extends ResultWithModel {

    private String datas;

    public OfflineFileData() {
    }

    public String getDatas() {
        return datas;
    }

    public void setDatas(String datas) {
        this.datas = datas;
    }
}
