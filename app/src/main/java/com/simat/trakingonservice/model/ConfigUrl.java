package com.simat.trakingonservice.model;

public class ConfigUrl {


   String CompanyCode;

    public ConfigUrl() {
    }

    public ConfigUrl(String companyCode) {
        CompanyCode = companyCode;
    }

    public String getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(String companyCode) {
        CompanyCode = companyCode;
    }
}
