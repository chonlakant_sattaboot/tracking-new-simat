package com.simat.trakingonservice.model;



public class UpdateDeviceLog {
    private String GPSProvider;
    private String ScreenStatus;

    public UpdateDeviceLog() {
    }

    public String getGPSProvider() {
        return GPSProvider;
    }

    public void setGPSProvider(String GPSProvider) {
        this.GPSProvider = GPSProvider;
    }

    public String getScreenStatus() {
        return ScreenStatus;
    }

    public void setScreenStatus(String screenStatus) {
        ScreenStatus = screenStatus;
    }
}
