package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 25/3/2559.
 */
public class DeviceStatus extends ResultWithModel {

    /**
     * Status : false
     * IsMaster : false
     * ShortName :
     * UUID : 357080050867945
     */

    private Datas datas;

    public Datas getDatas() {
        return datas;
    }

    public void setDatas(Datas datas) {
        this.datas = datas;
    }

    public static class Datas {
        private boolean Status;
        private boolean IsMaster;
        private String ShortName;
        private String UUID;
        private boolean IsLog;

        public boolean isStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public boolean isIsMaster() {
            return IsMaster;
        }

        public void setIsMaster(boolean IsMaster) {
            this.IsMaster = IsMaster;
        }

        public String getShortName() {
            return ShortName;
        }

        public void setShortName(String ShortName) {
            this.ShortName = ShortName;
        }

        public String getUUID() {
            return UUID;
        }

        public void setUUID(String UUID) {
            this.UUID = UUID;
        }

        public boolean isLog() {
            return IsLog;
        }

        public void setLog(boolean log) {
            IsLog = log;
        }
    }
}
