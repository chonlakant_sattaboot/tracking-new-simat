package com.simat.trakingonservice.model;

import android.content.Context;
import android.database.Cursor;

import com.simat.trakingonservice.database.TrackingProvider;


public class CTranModel {
    private TrackingModel trackingModel;

    public CTranModel(Context context) {
        super();
        trackingModel = new TrackingModel();
        NotifyTrackingModel(context);

    }

    public TrackingModel getTrackingModel() {
        return trackingModel;
    }

    public void setTrackingModel(TrackingModel trackingModel) {
        this.trackingModel = trackingModel;
    }

    public TrackingModel NotifyTrackingModel(Context context) {
        Cursor cursor = context.getContentResolver().query(TrackingProvider.TRACKING_CONTENT_URI, null, null, null, null);
        if(cursor != null){
            if(cursor.getCount() > 0){
                cursor.moveToFirst();


                trackingModel.setU_compID(cursor.getString(cursor.getColumnIndex("U_compID")));
                trackingModel.setU_HHID(cursor.getString(cursor.getColumnIndex("U_HHID")));
                trackingModel.setU_success(cursor.getString(cursor.getColumnIndex("U_success")));
                trackingModel.setU_message(cursor.getString(cursor.getColumnIndex("U_message")));
                trackingModel.setU_lat(cursor.getString(cursor.getColumnIndex("U_lat")));
                trackingModel.setU_lng(cursor.getString(cursor.getColumnIndex("U_lng")));

                trackingModel.setU_dlat(cursor.getString(cursor.getColumnIndex("U_dlat")));
                trackingModel.setU_dlng(cursor.getString(cursor.getColumnIndex("U_dlng")));

                trackingModel.setU_radius(cursor.getString(cursor.getColumnIndex("U_radius")));
                trackingModel.setU_battery(cursor.getString(cursor.getColumnIndex("U_battery")));
                trackingModel.setU_distance(cursor.getString(cursor.getColumnIndex("U_distance")));
                trackingModel.setU_poiID(cursor.getString(cursor.getColumnIndex("U_poiID")));

                trackingModel.setU_jobID(cursor.getString(cursor.getColumnIndex("U_jobID")));
                trackingModel.setU_checkInStatus(cursor.getString(cursor.getColumnIndex("U_checkInStatus")));
                trackingModel.setU_StatusID(cursor.getString(cursor.getColumnIndex("U_StatusID")));
                trackingModel.setU_StatusName(cursor.getString(cursor.getColumnIndex("U_StatusName")));

                trackingModel.setU_NaviOpen(cursor.getString(cursor.getColumnIndex("U_NaviOpen")));
                trackingModel.setU_NaviIsOpen(cursor.getString(cursor.getColumnIndex("U_NaviIsOpen")));
                trackingModel.setU_DestinationName(cursor.getString(cursor.getColumnIndex("U_DestinationName")));

                trackingModel.setU_clat(cursor.getString(cursor.getColumnIndex("U_clat")));
                trackingModel.setU_clng(cursor.getString(cursor.getColumnIndex("U_clng")));
                trackingModel.setU_cjobID(cursor.getString(cursor.getColumnIndex("U_cjobID")));
                trackingModel.setU_cjobStatus(cursor.getString(cursor.getColumnIndex("U_cjobStatus")));
                trackingModel.setU_ajobStatus(cursor.getString(cursor.getColumnIndex("U_ajobStatus"))) ;

            }
        }
        cursor.close();

        return trackingModel;

    }

}
