package com.simat.trakingonservice.model;

import android.annotation.SuppressLint;
import android.text.format.DateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

public class DateTime {
    private Timer timer;
    private Calendar calendar;

    public DateTime() {
        timer = new Timer();
    }

    public DateTime(int year, int month, int day, int hourOfDay, int minute,
                    int second) {
        timer = new Timer();
        this.calendar = new GregorianCalendar(year, month, day, hourOfDay,
                minute, second);
        this.timer.scheduleAtFixedRate(timerTask, 0, 1000L);
    }

    public String getDate() {
        String date = "";
        try {
            date = (String) DateFormat.format("yyyy-MM-dd kk:mm:ss",
                    this.calendar.getTime());
        } catch (Exception exception) {
            exception.toString();
        }
        return date;
    }

    @SuppressLint("SimpleDateFormat")
    public long MinDifferentDateTime(Date LastLocationUpdate, Date CurrentDate) {
        try {

            // in milliseconds
            long diff = CurrentDate.getTime() - LastLocationUpdate.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");

            return diffMinutes;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    public Calendar getCalendar() {
        return calendar;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    private TimerTask timerTask = new TimerTask() {

        @Override
        public void run() {
            calendar.add(Calendar.SECOND, 1);

        }
    };
}
