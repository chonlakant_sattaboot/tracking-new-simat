package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 12/16/2015.
 */
public class ConfigModel extends ResultWithModel {

    /**
     * BluetoothID :
     * BatteryLowLimit : 15
     * SpeedLimit : 120
     * LauncherPassword : 55555
     * AlarmDelayTime : 5
     * Location : {"Lat":13.756059440177216,"Lng":100.50174951553345}
     */

    private Datas datas;


    public Datas getDatas() {
        return datas;
    }

    public void setDatas(Datas datas) {
        this.datas = datas;
    }

    public static class Datas {
        private String BluetoothID;
        private int BatteryLowLimit;
        private int SpeedLimit;
        private String LauncherPassword;
        private int AlarmDelayTime;
        /**
         * Lat : 13.756059440177216
         * Lng : 100.50174951553345
         */
//        "UpdateLast": "2016-06-01T12:27:28.0847502+07:00",
//                "refCode": "Z8FEFW"

        private String UpdateLast;
        private String refCode;
        private LocationEntity Location;

        public String getBluetoothID() {
            return BluetoothID;
        }

        public void setBluetoothID(String BluetoothID) {
            this.BluetoothID = BluetoothID;
        }

        public int getBatteryLowLimit() {
            return BatteryLowLimit;
        }

        public void setBatteryLowLimit(int BatteryLowLimit) {
            this.BatteryLowLimit = BatteryLowLimit;
        }

        public int getSpeedLimit() {
            return SpeedLimit;
        }

        public void setSpeedLimit(int SpeedLimit) {
            this.SpeedLimit = SpeedLimit;
        }

        public String getLauncherPassword() {
            return LauncherPassword;
        }

        public void setLauncherPassword(String LauncherPassword) {
            this.LauncherPassword = LauncherPassword;
        }

        public int getAlarmDelayTime() {
            return AlarmDelayTime;
        }

        public void setAlarmDelayTime(int AlarmDelayTime) {
            this.AlarmDelayTime = AlarmDelayTime;
        }

        public String getRefCode() {
            return refCode;
        }

        public void setRefCode(String refCode) {
            this.refCode = refCode;
        }

        public String getUpdateLast() {
            return UpdateLast;
        }

        public void setUpdateLast(String updateLast) {
            UpdateLast = updateLast;
        }

        public LocationEntity getLocation() {
            return Location;
        }

        public void setLocation(LocationEntity Location) {
            this.Location = Location;
        }

        public static class LocationEntity {
            private double Lat;
            private double Lng;

            public double getLat() {
                return Lat;
            }

            public void setLat(double Lat) {
                this.Lat = Lat;
            }

            public double getLng() {
                return Lng;
            }

            public void setLng(double Lng) {
                this.Lng = Lng;
            }
        }
    }
}
