package com.simat.trakingonservice.model;

import org.parceler.Parcel;

/**
 * Created by NileZia on 12/21/2015.
 */

@Parcel
public class ProfileModel extends ResultWithModel {

    public ProfileModel() {
    }

    Datas datas;

    public Datas getDatas() {

        return datas == null ? new Datas() : datas;

    }

    public void setDatas(Datas datas) {
        this.datas = datas;
    }

    public static class Datas {

        boolean IsMaster;
        String CompanyID;
        String HHID;
        String UUID;
        String ShortName;
        String CompanyName;
        String LicenseExpireDate;
        private String Base;
        private String Soap;
        private String Mobile;
        private String Interface;
        private String Node;
        private String NodeApi;
        private String Socket;
        private String SocketPort;
        boolean Status;

        public String getCompanyID() {
            return CompanyID == null ? "" : CompanyID;
        }

        public void setCompanyID(String companyID) {
            CompanyID = companyID;
        }

        public boolean IsMaster() {
            return IsMaster;
        }

        public void setIsMaster(boolean isMaster) {
            IsMaster = isMaster;
        }

        public String getHHID() {
            return HHID == null ? "" : HHID;
        }

        public void setHHID(String HHID) {
            this.HHID = HHID;
        }

        public String getUUID() {
            return UUID == null ? "" : UUID;
        }

        public void setUUID(String UUID) {
            this.UUID = UUID;
        }

        public String getCompanyName() {
            return CompanyName == null ? "" : CompanyName;
        }

        public void setCompanyName(String companyName) {
            CompanyName = companyName;
        }

        public String getLicenseExpireDate() {
            return LicenseExpireDate == null ? "" : LicenseExpireDate;
        }

        public void setLicenseExpireDate(String licenseExpireDate) {
            LicenseExpireDate = licenseExpireDate;
        }

        public String getShortName() {
            return ShortName == null ? "" : ShortName;
        }

        public void setShortName(String shortName) {
            ShortName = shortName;
        }

        public boolean isStatus() {
            return Status;
        }

        public void setStatus(boolean status) {
            Status = status;
        }

        public String getBase() {
            return Base;
        }

        public void setBase(String base) {
            Base = base;
        }

        public String getSoap() {
            return Soap;
        }

        public void setSoap(String soap) {
            Soap = soap;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String mobile) {
            Mobile = mobile;
        }

        public String getInterface() {
            return Interface;
        }

        public void setInterface(String anInterface) {
            Interface = anInterface;
        }

        public String getNode() {
            return Node;
        }

        public void setNode(String node) {
            Node = node;
        }

        public String getNodeApi() {
            return NodeApi;
        }

        public void setNodeApi(String nodeApi) {
            NodeApi = nodeApi;
        }

        public String getSocket() {
            return Socket;
        }

        public void setSocket(String socket) {
            Socket = socket;
        }

        public String getSocketPort() {
            return SocketPort;
        }

        public void setSocketPort(String socketPort) {
            SocketPort = socketPort;
        }
    }

}
