package com.simat.trakingonservice.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by NileZia on 11/1/2559.
 */
public class ResultWithModel {

    @SerializedName("success")private boolean Success;
    @SerializedName("message")private String Message;
    @SerializedName("refcode")private String RefCode;
    @SerializedName("total")private int total;
    public ResultWithModel() {

    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean success) {
        Success = success;
    }

    public String getMessage() {

        return Message == null ? "" : Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getRefCode() {

        return RefCode == null ? "" : RefCode;
    }

    public void setRefCode(String refCode) {
        RefCode = refCode;
    }


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


    private void SerializationResult(String response) {

        try {

            JSONObject jsonResponse = new JSONObject(response);
            boolean isSuccess = jsonResponse.getBoolean("success");
            String message = jsonResponse.getString("message");
            String ref = jsonResponse.getString("refcode");
            String Datas = "";
            int total = 0;

            if (!jsonResponse.isNull("total")) {

                total = jsonResponse.getInt("total");
            }


            if (!jsonResponse.isNull("datas")) {

                Datas = jsonResponse.getJSONObject("datas").toString();
            }

            this.setMessage(message);
            this.setSuccess(isSuccess);
            this.setRefCode(ref);
            //this.setDatas(Datas);
            this.setTotal(total);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
