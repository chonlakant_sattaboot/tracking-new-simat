package com.simat.trakingonservice.model;

public class ResultConfigUrl {


    /**
     * success : true
     * message : null
     * refcode : 200
     * total : 0
     * datas : {"CompanyCode":"00008               ","Base":"https://skyfrog.io","Soap":"http://soap.skyfrog.io","Mobile":"http://mobile.skyfrog.io","Interface":"https://interface.skyfrog.io","Node":"https://node.skyfrog.io","NodeApi":"https://nodeapi.skyfrog.io","Socket":"socket.skyfrog.io:1301","IsActive":true}
     */

    private boolean success;
    private Object message;
    private String refcode;
    private int total;
    private DatasBean datas;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getRefcode() {
        return refcode;
    }

    public void setRefcode(String refcode) {
        this.refcode = refcode;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public DatasBean getDatas() {
        return datas;
    }

    public void setDatas(DatasBean datas) {
        this.datas = datas;
    }

    public static class DatasBean {
        /**
         * CompanyCode : 00008
         * Base : https://skyfrog.io
         * Soap : http://soap.skyfrog.io
         * Mobile : http://mobile.skyfrog.io
         * Interface : https://interface.skyfrog.io
         * Node : https://node.skyfrog.io
         * NodeApi : https://nodeapi.skyfrog.io
         * Socket : socket.skyfrog.io:1301
         * IsActive : true
         */

        private String CompanyCode;
        private String Base;
        private String Soap;
        private String Mobile;
        private String Interface;
        private String Node;
        private String NodeApi;
        private String Socket;
        private boolean IsActive;

        public String getCompanyCode() {
            return CompanyCode;
        }

        public void setCompanyCode(String CompanyCode) {
            this.CompanyCode = CompanyCode;
        }

        public String getBase() {
            return Base;
        }

        public void setBase(String Base) {
            this.Base = Base;
        }

        public String getSoap() {
            return Soap;
        }

        public void setSoap(String Soap) {
            this.Soap = Soap;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getInterface() {
            return Interface;
        }

        public void setInterface(String Interface) {
            this.Interface = Interface;
        }

        public String getNode() {
            return Node;
        }

        public void setNode(String Node) {
            this.Node = Node;
        }

        public String getNodeApi() {
            return NodeApi;
        }

        public void setNodeApi(String NodeApi) {
            this.NodeApi = NodeApi;
        }

        public String getSocket() {
            return Socket;
        }

        public void setSocket(String Socket) {
            this.Socket = Socket;
        }

        public boolean isIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }
    }
}
