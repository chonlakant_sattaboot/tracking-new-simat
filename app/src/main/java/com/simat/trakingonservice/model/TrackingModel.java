package com.simat.trakingonservice.model;

public class TrackingModel {
    private String U_compID;
    private String U_HHID;
    private String U_success;
    private String U_message;
    private String U_lat;
    private String U_lng;

    private String U_dlat;
    private String U_dlng;

    private String U_radius;
    private String U_battery;
    private String U_distance;
    private String U_poiID;

    private String U_jobID;
    private String U_checkInStatus;
    private String U_StatusID;
    private String U_StatusName;

    private String U_NaviOpen;
    private String U_NaviIsOpen;
    private String U_DestinationName;

    private String U_clat;
    private String U_clng;
    private String U_cjobID;
    private String U_cjobStatus;
    private String U_ajobStatus;


    public void setU_compID(String u_compID) {
        U_compID = u_compID;
    }

    public void setU_HHID(String u_HHID) {
        U_HHID = u_HHID;
    }

    public void setU_success(String u_success) {
        U_success = u_success;
    }

    public void setU_message(String u_message) {
        U_message = u_message;
    }

    public void setU_lat(String u_lat) {
        U_lat = u_lat;
    }

    public void setU_lng(String u_lng) {
        U_lng = u_lng;
    }

    public void setU_dlat(String u_dlat) {
        U_dlat = u_dlat;
    }

    public void setU_dlng(String u_dlng) {
        U_dlng = u_dlng;
    }

    public void setU_radius(String u_radius) {
        U_radius = u_radius;
    }

    public void setU_battery(String u_battery) {
        U_battery = u_battery;
    }

    public void setU_distance(String u_distance) {
        U_distance = u_distance;
    }

    public void setU_poiID(String u_poiID) {
        U_poiID = u_poiID;
    }

    public void setU_jobID(String u_jobID) {
        U_jobID = u_jobID;
    }

    public void setU_checkInStatus(String u_checkInStatus) {
        U_checkInStatus = u_checkInStatus;
    }

    public void setU_StatusID(String u_StatusID) {
        U_StatusID = u_StatusID;
    }

    public void setU_StatusName(String u_StatusName) {
        U_StatusName = u_StatusName;
    }

    public void setU_NaviOpen(String u_NaviOpen) {
        U_NaviOpen = u_NaviOpen;
    }

    public void setU_NaviIsOpen(String u_NaviIsOpen) {
        U_NaviIsOpen = u_NaviIsOpen;
    }

    public void setU_DestinationName(String u_DestinationName) {
        U_DestinationName = u_DestinationName;
    }

    public void setU_clat(String u_clat) {
        U_clat = u_clat;
    }

    public void setU_clng(String u_clng) {
        U_clng = u_clng;
    }

    public void setU_cjobID(String u_cjobID) {
        U_cjobID = u_cjobID;
    }

    public void setU_cjobStatus(String u_cjobStatus) {
        U_cjobStatus = u_cjobStatus;
    }

    public void setU_ajobStatus(String u_ajobStatus) {
        U_ajobStatus = u_ajobStatus;
    }

    public String getU_compID() {
        if (U_compID == null)
            U_compID = "";
        return U_compID;
    }

    public String getU_HHID() {
        if (U_HHID == null)
            U_HHID = "";
        return U_HHID;
    }

    public String getU_success() {
        if (U_success == null)
            U_success = "";
        return U_success;
    }

    public String getU_message() {
        if (U_message == null)
            U_message = "";
        return U_message;
    }

    public String getU_lat() {
        if (U_lat == null)
            U_lat = "";
        return U_lat;
    }

    public String getU_lng() {
        if (U_lng == null)
            U_lng = "";
        return U_lng;
    }

    public String getU_dlat() {
        if (U_dlat == null)
            U_dlat = "";
        return U_dlat;
    }

    public String getU_dlng() {
        if (U_dlng == null)
            U_dlng = "";
        return U_dlng;
    }

    public String getU_radius() {
        if (U_radius == null)
            U_radius = "";
        return U_radius;
    }

    public String getU_battery() {
        if (U_battery == null)
            U_battery = "";
        return U_battery;
    }

    public String getU_distance() {
        if (U_distance == null)
            U_distance = "";
        return U_distance;
    }

    public String getU_poiID() {
        if (U_poiID == null)
            U_poiID = "";
        return U_poiID;
    }

    public String getU_jobID() {
        if (U_jobID == null)
            U_jobID = "";
        return U_jobID;
    }

    public String getU_checkInStatus() {
        if (U_checkInStatus == null)
            U_checkInStatus = "";
        return U_checkInStatus;
    }

    public String getU_StatusID() {
        if (U_StatusID == null)
            U_StatusID = "";
        return U_StatusID;
    }

    public String getU_StatusName() {
        if (U_StatusName == null)
            U_StatusName = "";
        return U_StatusName;
    }

    public String getU_NaviOpen() {
        if (U_NaviOpen == null)
            U_NaviOpen = "";
        return U_NaviOpen;
    }

    public String getU_NaviIsOpen() {
        if (U_NaviIsOpen == null)
            U_NaviIsOpen = "";
        return U_NaviIsOpen;
    }

    public String getU_DestinationName() {
        if (U_DestinationName == null)
            U_DestinationName = "";
        return U_DestinationName;
    }

    public String getU_clat() {
        if (U_clat == null)
            U_clat = "";
        return U_clat;
    }

    public String getU_clng() {
        if (U_clng == null)
            U_clng = "";
        return U_clng;
    }

    public String getU_cjobID() {
        if (U_cjobID == null)
            U_cjobID = "";
        return U_cjobID;
    }

    public String getU_cjobStatus() {
        if (U_cjobStatus == null)
            U_cjobStatus = "";
        return U_cjobStatus;
    }

    public String getU_ajobStatus() {
        if (U_ajobStatus == null)
            U_ajobStatus = "";
        return U_ajobStatus;
    }

}
