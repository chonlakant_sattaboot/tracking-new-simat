package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 7/13/2016.
 */
public class AuthenModel {


    /**
     * action : authen
     * data : {"company":"00008","type":"mobile","user":"0802576559","pass":"123456"}
     */

    private String action;
    /**
     * company : 00008
     * type : mobile
     * user : 0802576559
     * pass : 123456
     */

    private Data data;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private String company;
        private String type;
        private String user;
        private String pass;

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }
    }
}
