package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 15/3/2559.
 */
public class MTranModel {


    /**
     * No : 0
     * StatusUpdate : 2016-05-17 09:49:43
     * CreateDate : 2016-05-17 09:50:45
     * CustomerID : C150066
     * DataType : OFFLINE
     * JobNo : SI1605049983
     * HHID : 0952083970
     * IsStart : 0
     * Device : {"Action":"1","Engine":"2","Status":"S10001"}
     * GPS : {"Status":"2","Accuracy":15,"Heading":26,"Distance":51.424847,"Lat":13.8062605,"Lng":100.8194112,"Speed":25.935444,"Battery":96,"Date":"2016-05-17 09:50:45"}
     * Config : {"Speed":80,"Battery":20}
     */

    private int No;
    private String StatusUpdate;
    private String CreateDate;
    private String CustomerID;
    private String DataType;
    private String JobNo;
    private String HHID;
    private int IsStart;
    /**
     * Action : 1
     * Engine : 2
     * Status : S10001
     */

    private MTranModel.Device Device;
    /**
     * Status : 2
     * Accuracy : 15
     * Heading : 26
     * Distance : 51.424847
     * Lat : 13.8062605
     * Lng : 100.8194112
     * Speed : 25.935444
     * Battery : 96
     * Date : 2016-05-17 09:50:45
     */

    private MTranModel.GPS GPS;
    /**
     * Speed : 80
     * Battery : 20
     */

    private MTranModel.Config Config;

    public int getNo() {
        return No;
    }

    public void setNo(int No) {
        this.No = No;
    }

    public String getStatusUpdate() {
        return StatusUpdate;
    }

    public void setStatusUpdate(String StatusUpdate) {
        this.StatusUpdate = StatusUpdate;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String CreateDate) {
        this.CreateDate = CreateDate;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String CustomerID) {
        this.CustomerID = CustomerID;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String DataType) {
        this.DataType = DataType;
    }

    public String getJobNo() {
        return JobNo;
    }

    public void setJobNo(String JobNo) {
        this.JobNo = JobNo;
    }

    public String getHHID() {
        return HHID;
    }

    public void setHHID(String HHID) {
        this.HHID = HHID;
    }

    public int getIsStart() {
        return IsStart;
    }

    public void setIsStart(int IsStart) {
        this.IsStart = IsStart;
    }

    public MTranModel.Device getDevice() {
        return Device;
    }

    public void setDevice(MTranModel.Device Device) {
        this.Device = Device;
    }

    public MTranModel.GPS getGPS() {
        return GPS ;
    }

    public void setGPS(MTranModel.GPS GPS) {
        this.GPS = GPS;
    }

    public MTranModel.Config getConfig() {
        return Config;
    }

    public void setConfig(MTranModel.Config Config) {
        this.Config = Config;
    }


    public static class Device {
        private String Action;
        private String Engine;
        private String Status;

        public String getAction() {
            return Action;
        }

        public void setAction(String Action) {
            this.Action = Action;
        }

        public String getEngine() {
            return Engine;
        }

        public void setEngine(String Engine) {
            this.Engine = Engine;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }
    }

    public static class GPS {
        private String Status;
        private int Accuracy;
        private double Heading;
        private double Distance;
        private double Lat;
        private double Lng;
        private double Speed;
        private int Battery;
        private long Date;

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public int getAccuracy() {
            return Accuracy;
        }

        public void setAccuracy(int Accuracy) {
            this.Accuracy = Accuracy;
        }

        public double getHeading() {
            return Heading;
        }

        public void setHeading(int Heading) {
            this.Heading = Heading;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public double getLat() {
            return Lat;
        }

        public void setLat(double Lat) {
            this.Lat = Lat;
        }

        public double getLng() {
            return Lng;
        }

        public void setLng(double Lng) {
            this.Lng = Lng;
        }

        public double getSpeed() {
            return Speed;
        }

        public void setSpeed(double Speed) {
            this.Speed = Speed;
        }

        public int getBattery() {
            return Battery;
        }

        public void setBattery(int Battery) {
            this.Battery = Battery;
        }

        public long getDate() {
            return Date;
        }

        public void setDate(long Date) {
            this.Date = Date;
        }
    }

    public static class Config {
        private int Speed;
        private int Battery;

        public int getSpeed() {
            return Speed;
        }

        public void setSpeed(int Speed) {
            this.Speed = Speed;
        }

        public int getBattery() {
            return Battery;
        }

        public void setBattery(int Battery) {
            this.Battery = Battery;
        }
    }
}
