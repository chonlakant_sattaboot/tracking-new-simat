package com.simat.trakingonservice.model;

/**
 * Created by NileZia on 7/13/2016.
 */
public class GPSModel {


    private String action;
    private String data;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
