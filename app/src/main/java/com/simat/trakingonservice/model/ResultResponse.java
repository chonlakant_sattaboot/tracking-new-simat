package com.simat.trakingonservice.model;

/**
 * Created by Nougat on 1/3/2560.
 */

public class ResultResponse {
    /**
     * success : false
     * message : No token provided.
     */

    private boolean success;
    private String message;

    public ResultResponse() {
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
