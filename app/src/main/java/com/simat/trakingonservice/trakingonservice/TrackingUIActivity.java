package com.simat.trakingonservice.trakingonservice;


import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.simat.trakingonservice.LocationService;
import com.simat.trakingonservice.R;
import com.simat.trakingonservice.controller.MainApplication;
import com.simat.trakingonservice.database.SkyFrogProvider;
import com.simat.trakingonservice.database.TrackingProvider;
import com.simat.trakingonservice.location.BestLocationListener;
import com.simat.trakingonservice.manager.Contextor;
import com.simat.trakingonservice.manager.LocationManager;
import com.simat.trakingonservice.manager.TCPManager;
import com.simat.trakingonservice.model.CTranModel;
import com.simat.trakingonservice.model.Friend;
import com.simat.trakingonservice.util.ConfigInformation;
import com.simat.trakingonservice.util.Connectivity;
import com.simat.trakingonservice.util.DBHelper;
import com.simat.trakingonservice.util.DeviceUrl;
import com.simat.trakingonservice.util.LOG;
import com.simat.trakingonservice.util.Utility;
import com.simat.trakingonservice.util.constantUtil;

import java.io.File;
import java.util.List;


public class TrackingUIActivity extends AppCompatActivity {

    String[] perms = {"android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_PHONE_STATE", "android.permission.WRITE_EXTERNAL_STORAGE"};

    final String PREF_NAME_URL = "UrlPreferencesTracking";
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    public static final String PREFS_NAME = "MyPrefsFile";
    public static String LED_RECEIVER = "com.simat.tracking.LED_RECEIVER";
    private ImageView imgPin;
    private ImageView imgSpot;
    private ImageView imgDashboard;
    private ImageView imageView4;
    private ImageView imageView7;
    private ImageView imgAcknowledge;
    private ImageView imageView10;
    private ImageButton imgResetMile;
    private ImageButton imgSetting;
    private ImageButton imgDigital;
    private TextView tvSpeedLimit;
    private TextView tvAcc;
    private TextView tvLat;
    private TextView tvLng;
    private TextView tvDistKM;
    private TextView texview6;
    private TextView tvDistM;
    private TextView tvProvider;
    private PopupWindow popupWindow1;
    private double Gps_speed = 0;
    private long distM = 0;

    private int distN[] = new int[20];
    private int i = 0, j = 0;
    private int st_com = 1;

    private boolean popup2st = false;

    private String LAT;
    private String LNG;
    private String key;

    int permsRequestCode = 200;

    private int LOCATION_ACCESS_REQUEST = 1001;
    private int IGNORE_OPTIMIZATION_REQUEST = 1002;
    private double OldSpeed = 0;
    private String verions;

    private LinearLayout layout;
    private ConfigInformation configInformation;
    private LocationManager locationManager;

    DBHelper mHelper;
    sendDataOffline sendDataOffline;
    int count = 0;
    private boolean connected = false;

    public BroadcastReceiver ledReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra("status", 0);
            UpdateViewLED(status);


            if (isNetworkConnected(context)) {
                //do action here
                if (!connected) {
                    Toast.makeText(context, "Internet Available!", Toast.LENGTH_SHORT).show();
                    // TCPManager.getInstance().start();
                    sendDataOffline = new sendDataOffline();
                    new sendDataOffline().execute();

                    WakeLocker.acquire(context);
                    WakeLocker.release();

                }
                connected = true;
            } else {
                if (connected) {
                    Toast.makeText(context, "Internet Unavailable!", Toast.LENGTH_SHORT).show();
                }
                connected = false;
            }


        }
    };

    private MainApplication trackingController;
    private BestLocationListener listener = new BestLocationListener() {
        @Override
        public void onLocationUpdate(Location location, boolean isFresh) {

            updateView(location);
            Log.d("activityListener",
                    "Type :"
                            + " Lat :"
                            + location.getLatitude()
                            + " Long:"
                            + location.getLongitude()

            );
        }
    };


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_traking_ui);
        mHelper = new DBHelper(TrackingUIActivity.this);



        if (android.os.Build.VERSION.SDK_INT >= 23) {
            requestPermissions(perms, permsRequestCode);
        } else {

        }


        sp = Contextor.getInstance().getContext().getSharedPreferences(PREF_NAME_URL, Contextor.getInstance().getContext().MODE_PRIVATE);
        editor = sp.edit();
        //  TimeBus.getInstance().register(this);

        configInformation = new ConfigInformation();
        trackingController = (MainApplication) getApplication();
        doBindService();
        initInstance();
        initLocation();
        initDashboard();
        layout = new LinearLayout(this);
        tvAcc.setText("");
        tvLat.setText("");
        tvLng.setText("");
        tvProvider.setText("");
        tvDistKM.setText("  " + distN[2] + "  " + distN[1] + "  "
                + distN[0]);
        tvDistM.setText("  " + distN[6] + "   " + distN[5] + "   "
                + distN[4] + "   " + distN[3]);


        imgResetMile.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                resetMile();
            }

        });
        imgSetting.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View arg0) {

                Connectivity connectivity = new Connectivity();
                boolean isGpsEnable = connectivity.isGpsEnabled(getApplicationContext());
                if (!isGpsEnable) {
                    checkGps();
                    return;
                }

                CTranModel cTranModel = new CTranModel(getApplicationContext());
                boolean isSuccess;
                isSuccess = Boolean.parseBoolean(cTranModel.getTrackingModel().getU_success() == null ? "false" : cTranModel.getTrackingModel().getU_success());
                Log.e("isSuccess", isSuccess + "");

                if (!isSuccess) {
                    Intent i = new Intent(TrackingUIActivity.this, ActivationActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(TrackingUIActivity.this, UnActivationActivity.class);
                    startActivity(i);
                }

            }
        });

        imgDigital.setOnClickListener(new Button.OnClickListener() {
            LayoutInflater layoutInflater;
            View popupView;

            // PopupWindow popupWindow1;
            public void onClick(View arg0) {


                if (!popup2st) {
                    layoutInflater = (LayoutInflater) getBaseContext()
                            .getSystemService(LAYOUT_INFLATER_SERVICE);
                    popupView = layoutInflater.inflate(R.layout.popupview2,
                            null);
                    popupWindow1 = new PopupWindow(popupView,
                            LayoutParams.MATCH_PARENT,
                            LayoutParams.MATCH_PARENT, true);
                    popupWindow1.showAsDropDown(layout, 0, 0);// 0,-20);-160
                    imgDashboard.setVisibility(View.INVISIBLE);
                    imgSpot.setVisibility(View.INVISIBLE);
                    imgPin.setVisibility(View.INVISIBLE);
                    texview6 = (TextView) popupView
                            .findViewById(R.id.textView6);

                    popup2st = true;
                    texview6.setOnClickListener(new Button.OnClickListener() {
                        public void onClick(View arg0) {
                            if (popup2st) {
                                imgDashboard.setVisibility(View.VISIBLE);
                                imgSpot.setVisibility(View.VISIBLE);
                                imgPin.setVisibility(View.VISIBLE);
                                popupWindow1.dismiss();
                                popup2st = false;
                            }
                        }

                    });
                }

            }
        });


        imgAcknowledge.setOnClickListener(new OnClickListener() {
            String message = "";
            String success = "";

            public void onClick(View v) {

                ContentValues values = new ContentValues();
                values.put("U_NaviIsOpen", "Y");

                ContentResolver cr = getContentResolver();
                Cursor c = cr.query(TrackingProvider.TRACKING_CONTENT_URI, null, null, null, null);
                if (c != null) {
                    c.moveToFirst();
                    try {
                        success = c.getString(c.getColumnIndex("U_success"));
                        message = c.getString(c.getColumnIndex("U_message"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (c.getCount() == 0) {
                        cr.insert(TrackingProvider.TRACKING_CONTENT_URI, values);
                    } else {
                        cr.update(TrackingProvider.TRACKING_CONTENT_URI,
                                values, null, null);
                    }
                    c.close();
                }

                if (Boolean.parseBoolean(success)) {
                    String where = "U_ACK = 'Y'";
                    try {
                        Cursor cursor = getContentResolver().query(SkyFrogProvider.JOBH_CONTENT_URI, null, where, null, null);
                        if (cursor != null) {
                            if (cursor.getCount() != 0) {
                                cursor.moveToFirst();
                                String value = "";
                                String value1 = "";
                                String status;

                                status = cursor.getString(cursor.getColumnIndex("U_Status"));

                                switch (status) {
                                    case "B":
                                        value = ""
                                                + cursor.getString(cursor.getColumnIndex("U_LatSource"));
                                        value1 = ""
                                                + cursor.getString(cursor.getColumnIndex("U_LngSource"));
                                        break;
                                    case "R":
                                        value = ""
                                                + cursor.getString(cursor.getColumnIndex("U_LatDesctination"));
                                        value1 = ""
                                                + cursor.getString(cursor.getColumnIndex("U_LngDesctination"));
                                        break;

                                }


                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("google.navigation:q="
                                                + value + "," + value1));
                                i.setClassName(
                                        "com.google.android.apps.maps",
                                        "com.google.android.maps.MapsActivity");
                                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(i);


                            } else {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),
                                                "No job acknowledge.",
                                                Toast.LENGTH_LONG).show();

                                    }
                                });

                            }
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                        Log.d("error", e.toString());
                    }

                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplication(), message,
                                    Toast.LENGTH_LONG).show();

                        }
                    });
                }
            }
        });

        tvLat.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentToGGMap();
            }
        });

        tvLng.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentToGGMap();
            }
        });

        imageView7.setVisibility(View.INVISIBLE);

        ContentResolver cr = getContentResolver();
        String where = "U_ACK = 'Y'";

        try {
            Cursor cursor = cr.query(SkyFrogProvider.JOBH_CONTENT_URI, null,
                    where, null, null);
            if (cursor != null) {
                if (cursor.getCount() != 0) {

                    imgAcknowledge.setImageDrawable(getNavDrawable(R.drawable.navigation));

                } else {

                    imgAcknowledge.setImageDrawable(getNavDrawable(R.drawable.navigation2));

                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void initLocation() {
        locationManager = new LocationManager(this);

        locationManager.setLocationRequest(new LocationRequest().setFastestInterval(1000)
                .setInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY))
                .setBestLocationListener(listener).build();

    }

    private void initDashboard() {
        Bitmap myImg = BitmapFactory.decodeResource(getResources(),
                R.drawable.slide5);

        imageView4.scrollTo(444, 0);
        j = 444;
        i = -24;
        Matrix matrix = new Matrix();
        matrix.postRotate(i);
        Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(),
                myImg.getHeight(), matrix, true);
        imgPin.setImageBitmap(rotated);

        imgPin.setScaleType(ScaleType.CENTER);
    }

    private void initInstance() {
        imgPin = (ImageView) findViewById(R.id.img_pin);
        imgSpot = (ImageView) findViewById(R.id.img_spot);
        imgDashboard = (ImageView) findViewById(R.id.img_dashboard);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        imageView7 = (ImageView) findViewById(R.id.imageView7);
        imgAcknowledge = (ImageView) findViewById(R.id.ima_enaable_navi);
        imageView10 = (ImageView) findViewById(R.id.imageView10);

        tvSpeedLimit = (TextView) findViewById(R.id.tv_speed_limit);
        tvAcc = (TextView) findViewById(R.id.tv_acc);
        tvLat = (TextView) findViewById(R.id.tv_lat);
        tvLng = (TextView) findViewById(R.id.tv_long);
        tvDistKM = (TextView) findViewById(R.id.textView5);
        tvDistM = (TextView) findViewById(R.id.textView6);
        tvProvider = (TextView) findViewById(R.id.tv_provider);

        imgResetMile = (ImageButton) findViewById(R.id.img_reset);
        imgSetting = (ImageButton) findViewById(R.id.img_setting);
        imgDigital = (ImageButton) findViewById(R.id.btn_digital);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (locationManager != null)
            locationManager.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (locationManager != null)
            locationManager.disconnect();

    }


    public Drawable getNavDrawable(int id) {

        return ContextCompat.getDrawable(this, id);

    }

    private void IntentToGGMap() {
        // Creates an Intent that will load a map of San Francisco
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + LAT + "," + LNG);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //TimeBus.getInstance().unregister(this);
        unregisterReceiver(ledReceiver);

    }


    public void checkGps() {

        Utility.AlertDialog("Warning", "Please turn on location Access.", this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, LOCATION_ACCESS_REQUEST);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

    }

    private void doBindService() {


        IntentFilter intentFilter = new IntentFilter(LED_RECEIVER);
        registerReceiver(ledReceiver, intentFilter);

        Context context = getBaseContext();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            context.startForegroundService(new Intent(context, LocationService.class));
        } else {
            context.startService(new Intent(context, LocationService.class));
        }

    }


    public void resetMile() {

        DialogInterface.OnClickListener callPositive = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (trackingController.getMyService() != null)
                    trackingController.getMyService().setDisPlayDistance(0);
                distM = 0;
                for (int jj = 0; jj < 7; jj++)
                    distN[jj] = 0;
            }
        };

        DialogInterface.OnClickListener callNegative = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
        Utility.AlertDialog("Warning", "Do you want to reset Mileage ?", this, callPositive, callNegative);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        key = intent.getStringExtra("KEY");
        if (key != null) {
            setCall();
        }
        super.onNewIntent(intent);
    }

    public void setCall() {

        final Handler handlerTimer = new Handler();
        handlerTimer.postDelayed(new Runnable() {
            public void run() {
                if (key.equalsIgnoreCase("1")) {
                    if (trackingController.getMyService() != null)
                        trackingController.getMyService().setSTCallApp(0);
                }
            }
        }, 1000);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void updateView(Location location) {

        if (trackingController.getMyService() != null) {
            distM = trackingController.getMyService().getDisPlayDistance();
            Gps_speed = trackingController.getMyService().getSpeed();

//            if (!location.hasSpeed()) {
//                Gps_speed = trackingController.getMyService().getSpeed();
//            } else {
//                Gps_speed = location.getSpeed() * 3.6f;
//            }

        }

        String dist_s_buff = String.valueOf(distM);
        int jj = 0;
        for (int ii = dist_s_buff.length(); ii > 0; ii--) {
            String xb = dist_s_buff.substring(ii - 1, ii);
            distN[jj] = Integer.parseInt(xb);
            jj++;
        }

        String currentConfigSpeed = String.valueOf(configInformation.getSpeedLimit());


        tvSpeedLimit.setText("" + String.format("%s", currentConfigSpeed));

        tvDistKM.setText("  " + distN[2] + "  " + distN[1] + "  "
                + distN[0]);
        tvDistM.setText("  " + distN[6] + "   " + distN[5] + "   "
                + distN[4] + "   " + distN[3]);


        try {


            LAT = String.valueOf(
                    location.getLatitude()).substring(0, 8);
            LNG = String.valueOf(
                    location.getLongitude()).substring(0, 8);
            String heading = String.format("%.0f", location
                    .getAccuracy()) + " m.";

            tvLat.setText("  Lat:" + LAT);
            tvLng.setText("  Lng:" + LNG);

            tvAcc.setText("Acc: " + heading);

            tvProvider.setText("Provider: " + location.getProvider());

            if (OldSpeed > 15) {

                if (Gps_speed == 0) {
                    return;
                }
            }
            OldSpeed = Gps_speed;

        } catch (Exception e) {

            LOG log = new LOG(e.toString(), "error", getApplicationContext());
            log.WriteLog();
        }


        imgPin.setRotation((float) ((1.52 * Gps_speed) + (-0)));

        if (st_com == 0) {
            j += 2;
            if (j >= 887) {

                j = 0;
            }

            imageView4.scrollTo(j - (444), 0);
        } else if (st_com == 1) {
            j -= 2;
            if (j <= 0) {
                j = 887;
            }
            imageView4.scrollTo(j - (444), 0);
        }

        float buff1;
        int azimut = 0;
        float azb = azimut * 887;
        if (azb < j) {
            buff1 = j - azb;
            if (buff1 > 887 / 2)
                st_com = 0;
            else
                st_com = 1;
        } else {
            buff1 = azb - j;
            if (buff1 > 887 / 2)
                st_com = 1;
            else
                st_com = 0;
        }
        if (azb >= j - 2 && azb <= j + 2)
            st_com = 2;

        if (popup2st)
            texview6.setText("" + (int) Gps_speed);


    }

    protected void onPause() {

        SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong("distance_total", distM);
        editor.putLong("slide_point", distM);
        editor.apply();

        super.onPause();

    }

    public void CheckLocationAccess() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                Connectivity connectivity = new Connectivity();
                boolean isGpsEnable = connectivity.isGpsEnabled(getApplicationContext());
                if (!isGpsEnable) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkGps();
                        }
                    });
                }
            }
        });

        if (!t.isAlive()) {
            t.start();

        }
    }


    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= 23) {
            CheckOptimizeMode();
        }


        CheckLocationAccess();
        ContentResolver cr = getContentResolver();
        String where = "U_ACK = 'Y'";
        Cursor cursor = null;
        try {
            cursor = cr.query(SkyFrogProvider.JOBH_CONTENT_URI, null,
                    where, null, null);

            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    imgAcknowledge.setImageDrawable(getNavDrawable(R.drawable.navigation));

                } else {
                    imgAcknowledge.setImageDrawable(getNavDrawable(R.drawable.navigation2));
                }
            }
            if (cursor != null) {
                cursor.close();
            }


        } catch (Exception e) {
            if (cursor != null) {
                cursor.close();
            }
            e.printStackTrace();
        }


        SharedPreferences settings = getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        distM = settings.getLong("distance_total", 0);

    }


    private void CheckOptimizeMode() {
        final Intent intent = new Intent();
        String packageName = getPackageName();
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (!pm.isIgnoringBatteryOptimizations(packageName)) {

            Utility.AlertDialogWarning("โหมดเพิ่มประสิทธิภาพแบตเตอรี่ถูกเปิดอยู่", "คุณต้องการปิดโหมดเพิ่มประสิทธิภาพแบตเตอรี่หรือไม่?", this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                    startActivity(intent);
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

        }
    }


    public void UpdateViewLED(int status) {
        try {

           // Log.e("statusLED", status + "");

            if (status == 1) {

                imageView10.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.led_green_th));

            } else {

                imageView10.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.red_led_on_th));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == LOCATION_ACCESS_REQUEST)
                && (resultCode == RESULT_OK)) {

        } else if ((requestCode == IGNORE_OPTIMIZATION_REQUEST)
                && (resultCode == RESULT_OK)) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            boolean isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());
            if (isIgnoringBatteryOptimizations) {
                // Ignoring battery optimization
            } else {
                // Not ignoring battery optimization
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:
                boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean READ_PHONE_STATE = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean WRITE_EXTERNAL_STORAGE = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                break;

        }

    }


    private class sendDataOffline extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... voids) {

            Log.e("getAllMTranOffline", mHelper.getAllMTranOffline().size() + "");
            try {

                if (mHelper.getAllMTranOffline().size() > 0) {
                    List<Friend> friends = mHelper.getAllMTranOffline();

                    for (int i = 0; i < friends.size(); i++) {
                        Log.e("Json", friends.get(i).getFirstName());
                        count++;
                        TCPManager.getInstance().send(DeviceUrl.getInstance().getKEY_BASE_SOCKET(),
                                Integer.parseInt(DeviceUrl.getInstance().getKEY_BASE_SOCKET_POST()), friends.get(i).getFirstName(), new TCPManager.SendCallback() {
                                    @Override
                                    public void onSuccess(String message) {

                                        Log.e("WebsocketOffline", "onSuccess >>>  " + message);
                                    }

                                    @Override
                                    public void onFailed(String message) {

                                        Log.e("WebsocketOffline", "onFailed >>" + message);
                                    }
                                });
                        if (friends.size() == count) {
                            Log.e("End", "End");
                            mHelper.deleteAll();
                            count = 0;
                            File path = new File(constantUtil.pathTemp);
                            path.delete();
                            sendDataOffline.cancel(true);

                        }
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } catch (NumberFormatException ex) { // handle your exception

            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }



    public static boolean isNetworkConnected(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        } catch (Exception e) {
            return true;
        }
    }


}