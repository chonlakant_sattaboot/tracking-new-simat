package com.simat.trakingonservice.trakingonservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.simat.trakingonservice.R;
import com.simat.trakingonservice.controller.ActivationController;
import com.simat.trakingonservice.database.TrackingProvider;
import com.simat.trakingonservice.manager.Contextor;
import com.simat.trakingonservice.manager.HttpConfigManager;
import com.simat.trakingonservice.manager.HttpManagerActivation;
import com.simat.trakingonservice.model.ActivationModel;
import com.simat.trakingonservice.model.ConfigUrl;
import com.simat.trakingonservice.model.ProfileModel;
import com.simat.trakingonservice.model.ResultConfigUrl;
import com.simat.trakingonservice.util.DataInfo;
import com.simat.trakingonservice.util.GetConfigPre;
import com.simat.trakingonservice.util.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by NamNile on 16/1/2558.
 */
public class ActivationActivity extends Activity {


    final String PREF_NAME_URL = "UrlPreferencesTracking";
    SharedPreferences sp;
    SharedPreferences.Editor editor;


    private Button btn_start_config;
    private EditText edt_companycode;
    private ConfigUrl configUrl;
    private ProgressDialog progressDialog = null;

    private EditText edt_hhid;
    private Button btn_active;
    private ActivationModel mActivationModel;
    String url;
    private ProfileModel.Datas datas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_url);

        datas = new ProfileModel.Datas();
        sp = Contextor.getInstance().getContext().getSharedPreferences(PREF_NAME_URL, Contextor.getInstance().getContext().MODE_PRIVATE);
        editor = sp.edit();
        InitWidget();

        btn_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog
                        .show(ActivationActivity.this,
                                "",
                                "Please Wait...",
                                true);

                configUrl = new ConfigUrl();

                if (datas.getCompanyID() != null) {
                    configUrl.setCompanyCode(datas.getCompanyID());
                }

                DataInfo.setCodeCompany(edt_companycode.getText().toString());

                ConnectAPICOMPANY(edt_companycode.getText().toString());
            }
        });
    }


    private void ConnectAPI() {
        Call<ProfileModel> call = new HttpManagerActivation().getService().Activation(mActivationModel);

        try {

            call.enqueue(new CallActivation());

        } catch (Exception e) {
            Log.e("httpConnect", e.toString());
        }
    }

    private void InitWidget() {
        DataInfo dataInfo = new DataInfo();
        int position = dataInfo.getHHID().length();
        edt_hhid = (EditText) findViewById(R.id.edt_hhid);
        btn_active = (Button) findViewById(R.id.btn_active);
        edt_companycode = (EditText) findViewById(R.id.edt_companycode);
        edt_hhid.setText(dataInfo.getHHID());
        edt_hhid.setSelection(position);
        edt_companycode.setText(DataInfo.getCompanyID());

    }

    private void ConnectAPICOMPANY(String company) {

        Call<ResultConfigUrl> call = new HttpConfigManager().getService().GetConfigUrl(company);

        try {

            call.enqueue(new CallConfigCompany());

        } catch (Exception e) {
            Log.e("httpConnect", e.toString());
        }

    }

    class CallConfigCompany implements Callback<ResultConfigUrl> {


        @Override
        public void onResponse(Call<ResultConfigUrl> call, Response<ResultConfigUrl> response) {


            if (response.body().isSuccess()) {

                String currentString = response.body().getDatas().getSocket();
                String[] separated = currentString.split(":");
                String urlSocket = separated[0];
                String port = separated[1];
                int postSplit = Integer.parseInt(port);


                editor.putString("BASESOAP", response.body().getDatas().getSoap());
                editor.putString("BASESKYFROG", response.body().getDatas().getBase());
                editor.putString("NASESKYFRIGMOBILE", response.body().getDatas().getMobile());
                editor.putString("BASEINTERFACE", response.body().getDatas().getInterface());
                editor.putString("BASENODEAPI", response.body().getDatas().getNodeApi());
                editor.putString("BASENODE", response.body().getDatas().getNode());
                editor.putString("BASESOCKET", urlSocket);
                editor.putInt("POSTSOCKET", postSplit);
                editor.apply();
                editor.commit();


                ContentValues values = new ContentValues();
                values.put("KEY_BASE_SOAP", response.body().getDatas().getSoap());
                values.put("KEY_BASE_SKYFROG", response.body().getDatas().getBase());
                values.put("KEY_BASE_SKYFROG_MOBILE", response.body().getDatas().getMobile());
                values.put("KEY_BASE_INTERFACE", response.body().getDatas().getInterface());
                values.put("KEY_BASE_NODE_API", response.body().getDatas().getNodeApi());
                values.put("KEY_BASE_NODE", response.body().getDatas().getNode());
                values.put("KEY_BASE_SOCKET", urlSocket);
                values.put("KEY_BASE_SOCKET_POST", postSplit);
                values.put("U_PodExit", "1");
                values.put("KEY_CHECK_EDIT_URL", "");


                ContentResolver cr = getContentResolver();
                Cursor c = cr.query(TrackingProvider.TRACKING_CONTENT_URI, null, null, null, null);

                if (c != null) {
                    if (c.getCount() == 0) {
                        cr.insert(TrackingProvider.TRACKING_CONTENT_URI, values);
                    } else {
                        cr.update(TrackingProvider.TRACKING_CONTENT_URI, values, null, null);
                    }
                    c.close();
                }


                url = response.body().getDatas().getMobile();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        mActivationModel = new ActivationModel();
                        mActivationModel.setHHID(edt_hhid.getText().toString());
                        mActivationModel.setUUID(new Utility().getUUID());
                        mActivationModel.setUDID(new Utility().getDeviceId());
                        mActivationModel.setActivate(true);
                        ConnectAPI();
                    }
                }, 2000);


            } else {
                progressDialog.dismiss();
            }


        }

        @Override
        public void onFailure(Call<ResultConfigUrl> call, Throwable t) {

        }
    }

    class CallActivation implements Callback<ProfileModel> {

        @Override
        public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
            ActivationController ac = new ActivationController();
            try {

                if (response.isSuccessful()) {
                    ProfileModel profileModel = response.body();

                    if (response.body() != null) {
                        if (profileModel.isSuccess()) {
                            profileModel.getDatas().setHHID(mActivationModel.getHHID());
                            profileModel.getDatas().setUUID(mActivationModel.getUDID());
                            profileModel.getDatas().setBase(GetConfigPre.getInstance().getBaseSkyfrog());
                            profileModel.getDatas().setSoap(GetConfigPre.getInstance().getSoap());
                            profileModel.getDatas().setMobile(GetConfigPre.getInstance().getBaseSkyfrogMobile());
                            profileModel.getDatas().setInterface(GetConfigPre.getInstance().getBaseInterface());
                            profileModel.getDatas().setNode(GetConfigPre.getInstance().getBaseNode());
                            profileModel.getDatas().setNodeApi(GetConfigPre.getInstance().getBaseNodeApi());
                            profileModel.getDatas().setSocket(GetConfigPre.getInstance().getSocket());
                            profileModel.getDatas().setSocketPort(Integer.toString(GetConfigPre.getInstance().getSocketPort()));
                            ac.setProfileModel(profileModel);

                            Log.e("getSocketPort", GetConfigPre.getInstance().getSocketPort() + "");

                            ac.Activate();
                            Intent i = new Intent(ActivationActivity.this, UnActivationActivity.class);
                            startActivity(i);
                            finish();
                            Log.e("httpConnect", "" + profileModel.getMessage());


                        } else {

                            Utility.AlertDialog("Warning !", profileModel.getMessage(),
                                    ActivationActivity.this, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            progressDialog.dismiss();
                                            dialog.dismiss();
                                        }
                                    });
                            Log.e("httpConnect", "" + response.errorBody().toString());

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "ติดต่อ Server ไม่ได้", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Log.e("httpConnect", "" + response.errorBody().string());

                }
            } catch (Exception e) {

                Log.d("httpConnect", "Out" + e.toString());

            } finally {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        }

        @Override
        public void onFailure(Call<ProfileModel> call, Throwable t) {
            Log.e("onFailure Activation", t.getMessage());
//            Log.e("getMessage",t.getMessage());
//            Log.e("getLocalizedMessage",t.getLocalizedMessage());
            Utility.AlertDialog("Warning !",
                    "Connection has Fail, Please try again.", ActivationActivity.this,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });
            progressDialog.dismiss();
        }
    }


}
