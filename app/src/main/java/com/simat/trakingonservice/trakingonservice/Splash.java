package com.simat.trakingonservice.trakingonservice;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.simat.trakingonservice.R;

public class Splash extends Activity {
    String[] perms = {"android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_PHONE_STATE"
            , "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.CAMERA", "android.permission.READ_CONTACTS"};
    Handler handler;
    Runnable runnable;
    long delay_time;
    long time = 1000L;

    int permsRequestCode = 200;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashscreen);

        handler = new Handler();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            requestPermissions(perms, permsRequestCode);

        } else {

            runnable = new Runnable() {
                public void run() {

                    Intent intent = new Intent(Splash.this, TrackingUIActivity.class);
                    startActivity(intent);
                    finish();
                }
            };
            handler.post(runnable);
        }
    }

    public void onResume() {
        super.onResume();
        delay_time = time;
        handler.postDelayed(runnable, delay_time);
        time = System.currentTimeMillis();
    }

    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
        time = delay_time - (System.currentTimeMillis() - time);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean READ_PHONE_STATE = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean WRITE_EXTERNAL_STORAGE = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                boolean CAMERA = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                boolean CONTACTS = grantResults[4] == PackageManager.PERMISSION_GRANTED;

                if (READ_PHONE_STATE == true) {

                    runnable = new Runnable() {
                        public void run() {


                            Intent intent = new Intent(Splash.this, TrackingUIActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    };
                }

                break;

        }

    }
}