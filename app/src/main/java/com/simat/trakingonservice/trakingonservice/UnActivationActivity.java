package com.simat.trakingonservice.trakingonservice;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.simat.trakingonservice.R;
import com.simat.trakingonservice.controller.ActivationController;
import com.simat.trakingonservice.controller.MainApplication;
import com.simat.trakingonservice.database.TrackingProvider;
import com.simat.trakingonservice.manager.Contextor;
import com.simat.trakingonservice.manager.HttpConfigManager;
import com.simat.trakingonservice.manager.HttpManager;
import com.simat.trakingonservice.manager.HttpManagerActivation;
import com.simat.trakingonservice.model.ActivationModel;
import com.simat.trakingonservice.model.CTranModel;
import com.simat.trakingonservice.model.DeviceStatus;
import com.simat.trakingonservice.model.ProfileModel;
import com.simat.trakingonservice.model.ResultConfigUrl;
import com.simat.trakingonservice.util.ConfigInfo;
import com.simat.trakingonservice.util.DataInfo;
import com.simat.trakingonservice.util.GetConfigPre;
import com.simat.trakingonservice.util.Utility;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.simat.trakingonservice.util.constantUtil.CHECKSCREEN;
import static com.simat.trakingonservice.util.constantUtil.CHECKVALUE;


public class UnActivationActivity extends AppCompatActivity {

    final String PREF_NAME_URL = "UrlPreferencesTracking";
    SharedPreferences sp;
    SharedPreferences.Editor editor;


    private TextView tv_comID_value, tv_comname_value, tv_hhid_value, tv_uuid_value, tv_status_value, tv_expire_value, tv_version, tv_master, tv_short;
    private Button btn_Unactive, btn_refresh, btn_refresh_config;
    private ProfileModel.Datas datas;
    private ActivationModel mActivationModel;
    private ProgressDialog progressDialog = null;
    private RadioButton checkOn;
    private RadioButton checkOff;

    private RadioButton checkGoogle;
    private RadioButton checkManual;

    private RadioGroup radioGroup;


    private TextView tv_key_config_value;
    boolean checkLicense = false;

    private ProgressDialog progressDialogConfig = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unactivate);
        sp = Contextor.getInstance().getContext().getSharedPreferences(PREF_NAME_URL, Contextor.getInstance().getContext().MODE_PRIVATE);
        editor = sp.edit();

        InitWidget();
        initialData();
        CheckOnscreen();
        UpdateView();

        checkLicense = getIntent().getBooleanExtra("checkLicense", false);
        if (checkLicense != false) {
            alertExprieLicense();
        }

    }

    public void alertExprieLicense() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(UnActivationActivity.this);
        builder.setMessage("ExprieLicense")
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        progressDialog = ProgressDialog
                                .show(UnActivationActivity.this,
                                        "",
                                        "Please Wait...",
                                        true);


                        mActivationModel = new ActivationModel();
                        mActivationModel.setHHID(datas.getHHID());
                        mActivationModel.setUUID(new Utility().getUUID());
                        mActivationModel.setUDID(new Utility().getDeviceId());
                        mActivationModel.setActivate(false);

                        ConnectAPI();

                    }
                });
        builder.show();
    }

    private void InitWidget() {

        checkOn = (RadioButton) findViewById(R.id.radioChkon);
        checkOff = (RadioButton) findViewById(R.id.radioChkoff);

        checkGoogle = (RadioButton) findViewById(R.id.radioChkGG);
        checkManual = (RadioButton) findViewById(R.id.radioChkMT);


        radioGroup = (RadioGroup) findViewById(R.id.Group_speed);

        tv_comID_value = (TextView) findViewById(R.id.tv_comID_value);
        tv_comname_value = (TextView) findViewById(R.id.tv_comname_value);
        tv_hhid_value = (TextView) findViewById(R.id.tv_hhid_value);
        tv_uuid_value = (TextView) findViewById(R.id.tv_uuid_value);
        tv_expire_value = (TextView) findViewById(R.id.tv_expire_value);
        tv_status_value = (TextView) findViewById(R.id.tv_status_value);
        tv_master = (TextView) findViewById(R.id.tv_master_value);
        tv_short = (TextView) findViewById(R.id.tv_short_value);
        tv_key_config_value = (TextView) findViewById(R.id.tv_key_config_value);
        btn_Unactive = (Button) findViewById(R.id.btn_unactive);
        btn_refresh = (Button) findViewById(R.id.btn_refresh);
        btn_refresh_config = (Button) findViewById(R.id.btn_refresh_config);

        tv_version = (TextView) findViewById(R.id.tv_version_name_value);

        setupSpeedRadioGroup();

    }

    private void setupSpeedRadioGroup() {
        if (ConfigInfo.getGoogleServiceSpeed()) {
            checkGoogle.setChecked(true);
        } else {
            checkManual.setChecked(true);
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioChkGG:
                        ConfigInfo.setGoogleServiceSpeed(true);
                        break;
                    case R.id.radioChkMT:
                        ConfigInfo.setGoogleServiceSpeed(false);
                        break;

                }
            }
        });
    }

    private void initialData() {
        CTranModel cTranModel = new CTranModel(getApplication());

        datas = new ProfileModel.Datas();
        datas.setHHID(cTranModel.getTrackingModel().getU_HHID());
        datas.setUUID(DataInfo.getUUID());
        datas.setCompanyName(DataInfo.getCompanyName());
        datas.setCompanyID(DataInfo.getCompanyID());
        datas.setLicenseExpireDate(ConfigInfo.getExpire());
        datas.setIsMaster(ConfigInfo.isMaster());
        datas.setStatus(true);
        datas.setShortName(DataInfo.getShortName());
    }

    public void UpdateView() {
        checkOn = (RadioButton) findViewById(R.id.radioChkon);
        checkOff = (RadioButton) findViewById(R.id.radioChkoff);
        tv_comID_value.setText(datas.getCompanyID());
        tv_comname_value.setText(datas.getCompanyName());
        tv_hhid_value.setText(datas.getHHID());
        tv_uuid_value.setText(datas.getUUID());
        tv_expire_value.setText(datas.getLicenseExpireDate());
        tv_status_value.setText("Activated");
        String txt_master = datas.IsMaster() ? "Yes" : "No";
        tv_master.setText(txt_master);
        tv_version.setText(getVersion());
        tv_short.setText(datas.getShortName());


        tv_key_config_value.setText(ConfigInfo.getKeyConfig());
        btn_Unactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Utility.AlertDialogWarning("แจ้งเตือน", "คุณต้องการออกจากการลงทะเบียนใช่หรือไม่่", UnActivationActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        progressDialog = ProgressDialog
                                .show(UnActivationActivity.this,
                                        "",
                                        "Please Wait...",
                                        true);


                        mActivationModel = new ActivationModel();
                        mActivationModel.setHHID(datas.getHHID());
                        mActivationModel.setUUID(new Utility().getUUID());
                        mActivationModel.setUDID(new Utility().getDeviceId());
                        mActivationModel.setActivate(false);

                        Log.e("hhhhhhhh", datas.getCompanyID());

                        editor.putString("KEY_COMPANYCODE", datas.getCompanyID());
                        editor.apply();
                        editor.commit();


                        ConnectAPI();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });


            }
        });

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication trackingController;
                trackingController = (MainApplication) Contextor.getInstance().getContext();
                trackingController.getMyService().GetConfigTracking();
                CheckDeviceStatus();
            }
        });

        btn_refresh_config.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("RRRRR", datas.getHHID());

                progressDialogConfig = ProgressDialog
                        .show(UnActivationActivity.this,
                                "",
                                "Please Wait...",
                                true);
                progressDialogConfig.setCancelable(false);

                mActivationModel = new ActivationModel();
                mActivationModel.setHHID(datas.getHHID());
                mActivationModel.setUUID(new Utility().getUUID());
                mActivationModel.setUDID(new Utility().getDeviceId());
                mActivationModel.setActivate(false);

                ConnectAPIConfig();


            }
        });

        Cursor cursor = null;
        try {
            ContentResolver cr = getContentResolver();
            cursor = cr.query(Uri.parse("content://com.simat.trackingprovider/tracks"), null, null, null, null);
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();

                    Log.e("fffff", cursor.getString(cursor.getColumnIndex("KEY_CHECK_EDIT_URL")) + "ffffff");

                    if (TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex("KEY_CHECK_EDIT_URL"))) || cursor.getString(cursor.getColumnIndex("KEY_CHECK_EDIT_URL")) == null) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UnActivationActivity.this);
                        alertDialogBuilder.setTitle("อัพเดพสำเร็จ");
                        alertDialogBuilder
                                .setMessage("กระบวนการอัพเดพสำเร็จ.")
                                .setCancelable(false)
                                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();

                                        mActivationModel = new ActivationModel();
                                        mActivationModel.setHHID(datas.getHHID());
                                        mActivationModel.setUUID(new Utility().getUUID());
                                        mActivationModel.setUDID(new Utility().getDeviceId());
                                        mActivationModel.setActivate(false);

                                        ConnectAPIConfig();

                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }

                }
            }
            cursor.close();

        } catch (Exception e) {

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    RadioButton.OnClickListener myOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.radioChkon:
                    getPreferencesEditor(getApplicationContext()).putBoolean(CHECKVALUE, true).apply();

                    break;
                case R.id.radioChkoff:
                    getPreferencesEditor(getApplicationContext()).putBoolean(CHECKVALUE, false).apply();

                    break;
            }
        }
    };

    private void ConnectAPIConfig() {
        CTranModel cTranModel = new CTranModel(getApplicationContext());
        final String CompanyID = cTranModel.getTrackingModel().getU_compID();
        Call<ResultConfigUrl> callUrl = new HttpConfigManager().getService().GetConfigUrl(DataInfo.getCompanyID());

        try {
            callUrl.enqueue(new CallConfigUrl());
        } catch (Exception e) {
            Log.e("httpConnect", e.toString());
        }

    }


    class CallConfigUrl implements Callback<ResultConfigUrl> {
        @Override
        public void onResponse(Call<ResultConfigUrl> call, Response<ResultConfigUrl> response) {
            Log.e("response", response.raw() + "");

            if (response.body() != null) {
                if (response.body().isSuccess()) {

                    Toast.makeText(getApplicationContext(), "Update success", Toast.LENGTH_SHORT).show();

                    String currentString = response.body().getDatas().getSocket();
                    String[] separated = currentString.split(":");
                    String urlSocket = separated[0];
                    String port = separated[1];
                    int postSplit = Integer.parseInt(port);


                    editor.putString("BASESOAP", response.body().getDatas().getSoap());
                    editor.putString("BASESKYFROG", response.body().getDatas().getBase());
                    editor.putString("NASESKYFRIGMOBILE", response.body().getDatas().getMobile());
                    editor.putString("BASEINTERFACE", response.body().getDatas().getInterface());
                    editor.putString("BASENODEAPI", response.body().getDatas().getNodeApi());
                    editor.putString("BASENODE", response.body().getDatas().getNode());
                    editor.putString("BASESOCKET", urlSocket);
                    editor.putString("HHID", datas.getHHID());
                    editor.putInt("POSTSOCKET", postSplit);
                    editor.apply();
                    editor.commit();

                    ContentValues values = new ContentValues();
                    values.put("KEY_BASE_SOAP", response.body().getDatas().getSoap());
                    values.put("KEY_BASE_SKYFROG", response.body().getDatas().getBase());
                    values.put("KEY_BASE_SKYFROG_MOBILE", response.body().getDatas().getMobile());
                    values.put("KEY_BASE_INTERFACE", response.body().getDatas().getInterface());
                    values.put("KEY_BASE_NODE_API", response.body().getDatas().getNodeApi());
                    values.put("KEY_BASE_NODE", response.body().getDatas().getNode());
                    values.put("KEY_BASE_SOCKET", urlSocket);
                    values.put("KEY_BASE_SOCKET_POST", postSplit);
                    values.put("KEY_CHECK_EDIT_URL", "1");
                    values.put("U_success", "true");
                    values.put("U_PodExit", "1");
                    values.put("U_HHID", datas.getHHID());
                    Log.e("getBase", response.body().getDatas().getBase());

                    ContentResolver cr = getContentResolver();
                    Cursor c = cr.query(TrackingProvider.TRACKING_CONTENT_URI, null, null, null, null);

                    if (c != null) {
                        cr.update(TrackingProvider.TRACKING_CONTENT_URI, values, null, null);
                        c.close();
                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            mActivationModel = new ActivationModel();
                            mActivationModel.setHHID(sp.getString("HHID", datas.getHHID()));
                            mActivationModel.setUUID(new Utility().getUUID());
                            mActivationModel.setUDID(new Utility().getDeviceId());
                            mActivationModel.setActivate(false);

                            Log.e("HHHH", datas.getHHID());

                            if (progressDialogConfig != null) {
                                progressDialogConfig.dismiss();
                            }


                            setCheckLicense();

                        }
                    }, 300);


                } else {
                    Toast.makeText(getApplicationContext(), "Update failed", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (progressDialogConfig != null) {
                    progressDialogConfig.dismiss();
                }

                Toast.makeText(getApplicationContext(), "ติดต่อ Server ไม่ได้", Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        public void onFailure(Call<ResultConfigUrl> call, Throwable t) {

        }
    }

    private void CheckOnscreen() {


        boolean getcheck = getPreferences(this).getBoolean(CHECKVALUE, true);

        checkOn.setOnClickListener(myOnClickListener);
        checkOff.setOnClickListener(myOnClickListener);
        if (getcheck) {
            checkOn.setChecked(true);
            checkOff.setChecked(false);

        } else {
            checkOn.setChecked(false);
            checkOff.setChecked(true);

        }
    }

    private void ConnectAPI() {
        Call<ProfileModel> call = new HttpManagerActivation().getService().Activation(mActivationModel);
        call.enqueue(new CallUnActive());
    }


    public String getVersion() {

        String data = null;
        try {
            data = getApplication().getPackageManager().getPackageInfo(
                    getApplication().getPackageName(), 0).versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }

    ActivationController ac;

    private SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(CHECKSCREEN, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getPreferencesEditor(Context context) {
        return getPreferences(context).edit();
    }


    class CallUnActive implements Callback<ProfileModel> {
        ProfileModel profileModel;

        @Override
        public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
            ac = new ActivationController();
            //  Log.e("response", response.raw() + "");
            try {
                if (response.isSuccessful()) {
                    ProfileModel profileModel = response.body();
                    this.profileModel = profileModel;

                    if (profileModel.isSuccess()) {
                        ac.setProfileModel(profileModel);
                        ac.UnActivate();
                        GetConfigPre.getInstance().clear();

                        Intent i = new Intent(getApplicationContext(), TrackingUIActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        unSuccess();
                    }

                } else {
                    Log.d("httpConnect", "" + response.errorBody().string());
                }

            } catch (Exception e) {

                Log.d("httpConnect", "" + e.toString());

            } finally {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        }

        @Override
        public void onFailure(Call<ProfileModel> call, Throwable t) {

            Utility.AlertDialog("Warning !", "" + t.toString(), UnActivationActivity.this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            Log.d("onFailure", "" + t.toString());
            progressDialog.dismiss();
        }

        private void unSuccess() {
            switch (profileModel.getRefCode()) {

                case "00007":
                    ac.UnActivate();
                    break;
                case "00004":
                    ac.UnActivate();
                    break;
                case "00001":
                case "00002":
                case "00003":
                case "00005":
                case "00006":
                    break;
            }

            Utility.AlertDialog("Warning !", profileModel.getMessage(), UnActivationActivity.this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void CheckDeviceStatus() {

        Call<DeviceStatus> callProfile = HttpManager.getInstance().getService().DeviceStatus();
        callProfile.enqueue(
                new Callback<DeviceStatus>() {
                    @Override
                    public void onResponse(Call<DeviceStatus> call, Response<DeviceStatus> response) {
                        ActivationController ac = new ActivationController();
                        if (response.isSuccessful()) {
                            DeviceStatus deviceStatus = response.body();

                            if (deviceStatus.isSuccess()) {
                                DeviceStatus.Datas datas = deviceStatus.getDatas();
                                if (!datas.isStatus()) {

                                    ac.UnActivate();


                                } else {

                                    DataInfo.setShortName(datas.getShortName());
                                    ConfigInfo.setMaster(datas.isIsMaster());
                                    ConfigInfo.setLog(datas.isLog());

                                }

                            } else {
                                switch (deviceStatus.getRefCode()) {

                                    case "00004":
                                    case "00007":
                                        Log.e("inactived", "Error 00007 : " + " Inactive");
                                        ac.UnActivate();
                                        break;

                                }
                                Log.e("CheckDeviceStatus", deviceStatus.getRefCode() + " : " + deviceStatus.getMessage());
                            }
                        } else

                        {
                            try {
                                Log.e("CheckDeviceStatus", "CheckDeviceStatus : " + response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DeviceStatus> call, Throwable t) {
                        Log.e("CheckDeviceStatus", "CheckDeviceStatus : " + t.toString());

                    }
                }
        );
    }


    public void setCheckLicense() {

        ActivationModel mActivationLicense = new ActivationModel();
        mActivationLicense.setHHID(datas.getHHID());
        mActivationLicense.setUUID(new Utility().getUUID());
        mActivationLicense.setUDID(new Utility().getDeviceId());
        mActivationLicense.setActivate(true);

        Log.e("Check_test", datas.getHHID());
        Log.e("Check_test", (new Utility().getUUID()));
        Log.e("Check_test", new Utility().getDeviceId());


        Call<ProfileModel> call = new HttpManagerActivation().getService().Activation(mActivationLicense);
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {

                Log.e("response", response.raw() + "");

                ProfileModel profileModelLicense = response.body();

                if (response.body() != null) {
                    if (profileModelLicense.isSuccess()) {

                        Log.e("Check_test", "OK");
                        showDialog("อัพเดพข้อมูลสำเร็จ", "ตกลง", true);

                    } else {
                        Log.e("Check_test", "NO");
                        Log.e("Check_test", profileModelLicense.getRefCode());

                        ActivationController acLicense = new ActivationController();
                        switch (profileModelLicense.getRefCode()) {
                            case "00007":
                                acLicense.ClearData();
                                showDialog(profileModelLicense.getMessage(), "ตกลง", false);
                                break;
                            case "00004":
                                acLicense.ClearData();
                                showDialog(profileModelLicense.getMessage(), "ตกลง", false);
                                break;
                            case "00001":
                                showDialog(profileModelLicense.getMessage(), "ตกลง", true);
                                break;
                            case "00002":
                                showDialog(profileModelLicense.getMessage(), "ตกลง", true);
                                break;
                            case "00003":
                                showDialog(profileModelLicense.getMessage(), "ตกลง", true);
                                break;
                            case "00005":
                                showDialog(profileModelLicense.getMessage(), "ตกลง", true);
                                break;
                            case "00006":
                                showDialog(profileModelLicense.getMessage(), "ตกลง", true);
                                break;
                        }

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "ติดต่อ Server ไม่ได้", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {

            }
        });

    }

    public void showDialog(String message, String accept, final boolean isStatus) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UnActivationActivity.this);
        alertDialogBuilder.setTitle(message);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Log.e("isStatus", isStatus + " ");
                        if (isStatus) {
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            Intent i = new Intent(getApplicationContext(), TrackingUIActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
