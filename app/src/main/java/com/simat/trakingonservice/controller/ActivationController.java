package com.simat.trakingonservice.controller;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.simat.trakingonservice.database.TrackingProvider;
import com.simat.trakingonservice.manager.Contextor;
import com.simat.trakingonservice.manager.HttpManager;
import com.simat.trakingonservice.model.ProfileModel;
import com.simat.trakingonservice.util.ConfigInfo;
import com.simat.trakingonservice.util.DataInfo;


public class ActivationController {

    private Context mContext;
    private ProfileModel profileModel;

    public ActivationController() {

        this.mContext = Contextor.getInstance().getContext();

    }

    public void Activate() {


        ContentValues values = new ContentValues();
        values.put("U_compID", profileModel.getDatas().getCompanyID());
        values.put("U_HHID", profileModel.getDatas().getHHID());
        values.put("U_success", "true");
        values.put("U_message", profileModel.getMessage());




        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(TrackingProvider.TRACKING_CONTENT_URI, null, null, null, null);

        if (c != null) {
            if (c.getCount() == 0) {
                cr.insert(TrackingProvider.TRACKING_CONTENT_URI, values);
            } else {
                cr.update(TrackingProvider.TRACKING_CONTENT_URI, values, null, null);
            }
            c.close();
        }
        putData(profileModel.getDatas());

    }

    public void UnActivate() {

        ContentValues values = new ContentValues();
        values.put("U_compID", "");
        values.put("U_HHID", "");
        values.put("U_success", "false");
        values.put("U_message", "");

        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(TrackingProvider.TRACKING_CONTENT_URI, null, null, null, null);

        if (c != null) {
            if (c.getCount() == 0) {
                cr.insert(TrackingProvider.TRACKING_CONTENT_URI, values);
            } else {
                cr.update(TrackingProvider.TRACKING_CONTENT_URI, values, null, null);

            }
            c.close();
        }

        putData(new ProfileModel.Datas());


        Intent i = new Intent("com.simat.SKYFROG_GRAPH");
        mContext.sendBroadcast(i);

        Intent i2 = new Intent("com.simat.OFF");
        mContext.sendBroadcast(i2);


        DataInfo.clear();

        HttpManager.getInstance().Reset();

    }


    public void ClearData() {

        ContentValues values = new ContentValues();
        values.put("U_compID", "");
        values.put("U_HHID", "");
        values.put("U_success", "false");
        values.put("U_message", "");

        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(TrackingProvider.TRACKING_CONTENT_URI, null, null, null, null);

        if (c != null) {
            if (c.getCount() == 0) {
                cr.insert(TrackingProvider.TRACKING_CONTENT_URI, values);
            } else {
                cr.update(TrackingProvider.TRACKING_CONTENT_URI, values, null, null);

            }
            c.close();
        }

        putData(new ProfileModel.Datas());


        Intent i = new Intent("com.simat.SKYFROG_GRAPH");
        mContext.sendBroadcast(i);

        Intent i2 = new Intent("com.simat.OFF");
        mContext.sendBroadcast(i2);


        DataInfo.clear();

        HttpManager.getInstance().Reset();

    }

    private void putData(ProfileModel.Datas datas) {


        if (datas.getHHID().isEmpty()) {
            DataInfo.setHHID(DataInfo.getHHID());
        } else {

            DataInfo.setHHID(datas.getHHID());
            DataInfo.setCompanyID(datas.getCompanyID());
            DataInfo.setCompanyName(datas.getCompanyName());
            DataInfo.setStatus(true);
            DataInfo.setShortName(datas.getShortName());
            DataInfo.setUUID(datas.getUUID());

            ConfigInfo.setExpire(datas.getLicenseExpireDate());
            ConfigInfo.setMaster(datas.IsMaster());

        }

    }

    public void setProfileModel(ProfileModel profileModel) {
        this.profileModel = profileModel;
    }

}
