package com.simat.trakingonservice.controller;


import android.app.Application;

import com.simat.trakingonservice.LocationService;
import com.simat.trakingonservice.crash.ErrorReporter;
import com.simat.trakingonservice.manager.Contextor;
import com.simat.trakingonservice.util.DeviceUrl;
import com.simat.trakingonservice.util.GetConfigPre;

public class MainApplication extends Application {
    private LocationService myService;

    public LocationService getMyService() {
        return myService;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Contextor.getInstance().init(getApplicationContext());
        GetConfigPre.getInstance().init(getApplicationContext());
        DeviceUrl.getInstance();

//        ErrorReporter.get(this)
//                .setEmailAddresses("skyfrog.dev@gmail.com")
//                .setEmailSubject("Crash Report")
//                .start();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

    }

    public void setMyService(LocationService myService) {
        this.myService = myService;
    }


}
