package com.simat.trakingonservice.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.simat.trakingonservice.manager.Contextor;

import static com.simat.trakingonservice.util.DataInfo.PREF_STATE;


public class ConfigInfo {


    public static String getExpire() {
        return getPreferences(Contextor.getInstance().getContext()).getString("Expire", "-");
    }

    public static void setExpire(String expire) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("Expire", expire).apply();
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_STATE, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getPreferencesEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static boolean isLog() {
        return getPreferences(Contextor.getInstance().getContext()).getBoolean("is_log", false);
    }

    public static void setLog(boolean islog) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putBoolean("is_log", islog).apply();
    }

    public static boolean isMaster() {
        return getPreferences(Contextor.getInstance().getContext()).getBoolean("isMaster", false);
    }

    public static void setMaster(boolean master) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putBoolean("isMaster", master).apply();
    }

    public static String getUpdateLast() {
        return getPreferences(Contextor.getInstance().getContext()).getString("updateLast", "-");
    }

    public static void setKeyUpdateLast(String updateLast) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("updateLast", updateLast);
    }

    public static String getKeyConfig() {
        return getPreferences(Contextor.getInstance().getContext()).getString("key", "-");
    }

    public static void setKeyConfig(String KeyVersion) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("key", KeyVersion).apply();
    }

    public static boolean getGoogleServiceSpeed() {
        return getPreferences(Contextor.getInstance().getContext()).getBoolean("cal_speed", true);
    }

    public static void setGoogleServiceSpeed(boolean isGoogleServiceSpeed) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putBoolean("cal_speed", isGoogleServiceSpeed).apply();
    }


    public static void clear() {
        getPreferencesEditor(Contextor.getInstance().getContext()).remove("key").commit();
        getPreferencesEditor(Contextor.getInstance().getContext()).remove("Version").commit();

    }

}
