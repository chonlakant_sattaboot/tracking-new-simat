package com.simat.trakingonservice.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.simat.trakingonservice.manager.Contextor;

public class DataInfo {

    public static final String PREF_STATE = "state";


    public static String getHHID() {
        return getPreferences(Contextor.getInstance().getContext()).getString("HHID", "");
    }

    public static void setHHID(String companyName) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("HHID", companyName).apply();
    }

    public static String getCodeCompany() {
        return getPreferences(Contextor.getInstance().getContext()).getString("CodeCompany", "");
    }

    public static void setCodeCompany(String companyName) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("CodeCompany", companyName).apply();
    }

    public static String getCompanyName() {
        return getPreferences(Contextor.getInstance().getContext()).getString("CompanyName", "-");
    }

    public static void setCompanyName(String companyName) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("CompanyName", companyName).apply();
    }

    public static String getCompanyID() {
        return getPreferences(Contextor.getInstance().getContext()).getString("CompanyID", "-");
    }

    public static void setCompanyID(String companyID) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("CompanyID", companyID).apply();
    }


    public static String getShortName() {
        return getPreferences(Contextor.getInstance().getContext()).getString("ShortName", "-");
    }

    public static void setShortName(String shortName) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("ShortName", shortName).apply();
    }

    public static boolean isStatus() {
        return getPreferences(Contextor.getInstance().getContext()).getBoolean("Status", false);
    }

    public static void setStatus(boolean status) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putBoolean("Status", status).apply();
    }


    public static String getVersion() {
        return getPreferences(Contextor.getInstance().getContext()).getString("Version", "-");
    }

    public static void setVersion(String version) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("Version", version).apply();
    }


    public static String getUUID() {
        return getPreferences(Contextor.getInstance().getContext()).getString("UUID", new Utility().getDeviceId());
    }

    public static void setUUID(String uuid) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("UUID", uuid).apply();
    }


    public static String getScreenStatus() {
//        String Screen = "";
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT_WATCH) {
//            //isInteractive() call
//            PowerManager pm = (PowerManager) Contextor.getInstance().getContext().getSystemService(Context.POWER_SERVICE);
//            boolean isScreenOn = pm.isInteractive();
//
//            if (isScreenOn)
//                Screen = "On";
//            else
//                Screen = "Off";
//
//        }


        return getPreferences(Contextor.getInstance().getContext()).getString("screen_status", "Off");
    }

    public static void setScreenStatus(String status) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("screen_status", status).apply();
    }

    public static String getGPSProvide() {

        return getPreferences(Contextor.getInstance().getContext()).getString("gps_provider", Connectivity.getLocationMode());
    }

    public static void setGPSProvide(String provider) {
        getPreferencesEditor(Contextor.getInstance().getContext()).putString("gps_provider", provider).apply();
    }


    public static void clear() {
        getPreferencesEditor(Contextor.getInstance().getContext()).remove("key").commit();
        getPreferencesEditor(Contextor.getInstance().getContext()).remove("Version").commit();

    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_STATE, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getPreferencesEditor(Context context) {
        return getPreferences(context).edit();
    }
}
