package com.simat.trakingonservice.util;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.simat.trakingonservice.manager.Contextor;


public class Utility {
    private Context mContext;

    public Utility() {

        this.mContext = Contextor.getInstance().getContext();
    }

    public String getUUID() {
        TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String uuid = tManager.getDeviceId();
        if (uuid == null)
            return getDeviceId();

        return uuid;
    }

    public String getVersion() {

        String data = null;
        try {
            data = mContext.getPackageManager().getPackageInfo(
                    mContext.getPackageName(), 0).versionName;

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
        }
        return data;
    }

    public String getDeviceId() {
        return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void AlertDialog(String title, String message, Context context, DialogInterface.OnClickListener callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setNegativeButton("Close", callback);
        builder.show();
    }

    public static void AlertDialog(String title, String message, Context context, DialogInterface.OnClickListener callPositive, DialogInterface.OnClickListener callNegative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("OK", callPositive);
        builder.setNegativeButton("CLOSE", callNegative);
        builder.show();
    }

    public static void AlertDialogWarning(String title, String message, Context context, DialogInterface.OnClickListener callPositive, DialogInterface.OnClickListener callNegative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("ใช่", callPositive);
        builder.setNegativeButton("ไม่", callNegative);
        builder.show();
    }

    public static void AddLogcat(String title, String detail, Context context) {
        LOG log = new LOG(title, detail, context);
        log.WriteLog();
    }

    public static Boolean isNumberFormat(String substring) {

        try {
            Double.parseDouble(substring);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
