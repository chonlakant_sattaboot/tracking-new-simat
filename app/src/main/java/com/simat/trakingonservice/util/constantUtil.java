package com.simat.trakingonservice.util;

import android.os.Environment;

public final class constantUtil {

    // Constant namespace
    public static final String NAMESPACE = "http://www.skyfrog.net/";

    public static final String pathLog = Environment.getExternalStorageDirectory()
            + "/Android/data/com.simat.trakingonservice/log";
    public static final String pathTemp = Environment.getExternalStorageDirectory()
            + "/Android/data/com.simat.trakingonservice/temp";

    public static final String CHECKSCREEN = "CHECKSCREEN";
    public static final String CHECKVALUE = "CHECKVALUE";
    public static final String Username = "Sim@tS0ft";
    public static final String Password = "SkyFr0g@S0ft";
}
