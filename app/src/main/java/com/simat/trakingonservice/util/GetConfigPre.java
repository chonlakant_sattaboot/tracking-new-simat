package com.simat.trakingonservice.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.simat.trakingonservice.manager.Contextor;


public class GetConfigPre {
    private Context mContext;
    final String PREF_NAME_URL = "UrlPreferencesTracking";
    final String KEY_BASE_SOAP = "BASESOAP";
    final String KEY_BASE_SKYFROG = "BASESKYFROG";
    final String KEY_BASE_SKYFROG_MOBILE = "NASESKYFRIGMOBILE";
    final String KEY_BASE_INTERFACE = "BASEINTERFACE";
    final String KEY_BASE_NODE_API = "BASENODEAPI";
    final String KEY_BASE_NODE = "BASENODE";
    final String KEY_BASE_SOCKET = "BASESOCKET";
    final String PREF_IS_CHANGE = "ISChange";
    final String KEY_BASE_SOCKET_POST = "POSTSOCKET";
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    private static GetConfigPre instance;

    public static GetConfigPre getInstance() {
        if (instance == null)
            instance = new GetConfigPre();
        return instance;
    }

    public void init(Context context) {
        mContext = context;
    }

    private GetConfigPre() {
        // ConnectAPI();
        mContext = Contextor.getInstance().getContext();
        sp = Contextor.getInstance().getContext().getSharedPreferences(PREF_NAME_URL, Contextor.getInstance().getContext().MODE_PRIVATE);
        editor = sp.edit();
    }

    public String getSoap() {

        return sp.getString(KEY_BASE_SOAP, "http://soap.skyfrog.net") + "/webservice.asmx?WSDL";
    }

    public String getBaseSkyfrog() {

        return sp.getString(KEY_BASE_SKYFROG, "https://skyfrog.net") + "/store/";
    }

    public String getBaseSkyfrogMobile() {

        return sp.getString(KEY_BASE_SKYFROG_MOBILE, "http://mobile.skyfrog.net") + "/api/";
    }

    public String getBaseInterface() {

        return sp.getString(KEY_BASE_INTERFACE, "https://interface.skyfrog.net") + "/POD/api/";
    }

    public String getBaseNodeApi() {

        return sp.getString(KEY_BASE_NODE_API, "https://nodeapi.skyfrog.net") + "/api/";
    }

    public String getBaseNode() {

        return sp.getString(KEY_BASE_NODE, "https://node.skyfrog.net");
    }

    public String getSocket() {

        return sp.getString(KEY_BASE_SOCKET, "socket.skyfrog.net");
    }

    public int getSocketPort() {

        return sp.getInt(KEY_BASE_SOCKET_POST, 1301);
    }

    public boolean getISCheck() {

        return sp.getBoolean(PREF_IS_CHANGE, false);
    }

    public void clear() {

        editor.clear().apply();
    }




}
