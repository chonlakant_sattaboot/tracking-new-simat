package com.simat.trakingonservice.util;

import android.content.Context;
import android.text.format.DateFormat;

import com.simat.trakingonservice.model.CTranModel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class LOG {
	private String detail;
	private String filename;
	private String title;

	public String getDetail() {
		return detail;
	}

	public String getTitle() {
		return title;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public LOG() {

	}

	public LOG(String detail, String title, Context context) {

		String HHID = "";
		String DATE ="";
		try {
			String pattern = "yyyyMMdd";
			DATE = (String) DateFormat.format(pattern,
					new Date());
			CTranModel cTranModel = new CTranModel(context);
			HHID = cTranModel.getTrackingModel().getU_HHID();
			
			this.detail = detail;
			this.title = title;
			this.filename = HHID + "_" + DATE;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void WriteLog() {
		boolean isNewfile = false;
		File path = new File(constantUtil.pathLog);
		File file = new File(path, getFilename());
		if (!path.isDirectory()) {
			path.mkdirs();
		}
		if (!file.exists()) {
			try {
				file.createNewFile();
				isNewfile = true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			// BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
			buf.append("< " + getTitle() + " >");
			buf.append(getDetail());
			buf.newLine();
			buf.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
