package com.simat.trakingonservice.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.simat.trakingonservice.manager.Contextor;


public class ConfigInformation {


    public static final String PREF_CONFIG = "config";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Context mContext;

    public ConfigInformation() {
        this.mContext = Contextor.getInstance().getContext();

        preferences = mContext.getSharedPreferences(PREF_CONFIG, Context.MODE_PRIVATE);
        editor = preferences.edit();

    }

    public void setSpeedLimit(int speedLimit) {

        editor.putInt("speedLimit", speedLimit);
        apply();
    }

    public int getSpeedLimit() {
        return preferences.getInt("speedLimit", 120);
    }


    public int getAlarmDelayTime() {
        return preferences.getInt("alarmDelay", 0);
    }

    public void setAlarmDelayTime(int alarmDelayTime) {
        editor.putInt("alarmDelay", alarmDelayTime);
        apply();
    }

    private void apply() {
        editor.apply();
    }

}
