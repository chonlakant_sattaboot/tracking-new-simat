package com.simat.trakingonservice.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.simat.trakingonservice.manager.Contextor;

public class DeviceUrl {

    private String HHID;
    private String CompanyID;
    private String KEY_BASE_SOAP;
    private String KEY_BASE_SKYFROG;
    private String KEY_BASE_SKYFROG_MOBILE;
    private String KEY_BASE_INTERFACE;
    private String KEY_BASE_NODE_API;
    private String KEY_BASE_NODE;
    private String KEY_BASE_SOCKET;
    private String KEY_BASE_SOCKET_POST;
    private String KEY_CHECK_EDIT_URL;
    private String U_PodExit;

    private String KEY_URL_FUEL;


    private static DeviceUrl instance;

    public static DeviceUrl getInstance() {
        if (instance == null)
            instance = new DeviceUrl();
        return instance;
    }

    private Context mContext;

    private DeviceUrl() {
        mContext = Contextor.getInstance().getContext();
    }

    public void InitData() {
        GetConfigPre.getInstance().init(mContext);
        Cursor cursor = null;
        try {
            ContentResolver cr = mContext.getContentResolver();
            cursor = cr.query(Uri.parse("content://com.simat.trackingprovider/tracks"), null, null, null, null);
            if (cursor != null) {
                if (cursor.getCount() != 0) {
                    cursor.moveToFirst();


                    setKEY_BASE_SOAP(cursor.getString(cursor.getColumnIndex("KEY_BASE_SOAP")));
                    setKEY_BASE_SKYFROG(cursor.getString(cursor.getColumnIndex("KEY_BASE_SKYFROG")));
                    setKEY_BASE_SKYFROG_MOBILE(cursor.getString(cursor.getColumnIndex("KEY_BASE_SKYFROG_MOBILE")));
                    setKEY_BASE_INTERFACE(cursor.getString(cursor.getColumnIndex("KEY_BASE_INTERFACE")));
                    setKEY_BASE_NODE_API(cursor.getString(cursor.getColumnIndex("KEY_BASE_NODE_API")));
                    setKEY_BASE_NODE(cursor.getString(cursor.getColumnIndex("KEY_BASE_NODE")));
                    setKEY_BASE_SOCKET(cursor.getString(cursor.getColumnIndex("KEY_BASE_SOCKET")));
                    setKEY_BASE_SOCKET_POST(cursor.getString(cursor.getColumnIndex("KEY_BASE_SOCKET_POST")));
                    setKEY_URL_FUEL(cursor.getString(cursor.getColumnIndex("KEY_BASE_SKYFROG")));
                    setU_PodExit(cursor.getString(cursor.getColumnIndex("U_PodExit")));

                }
                Log.e("KEY_BASE_SKYFROG_MOBILE", cursor.getString(cursor.getColumnIndex("KEY_BASE_SKYFROG_MOBILE")) + "มาไหม");
            }
            cursor.close();

        } catch (Exception e) {

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public boolean verificationSoap(String _username) {
        ContentResolver cr = mContext.getContentResolver();
        Cursor cursor = cr.query(Uri.parse("content://com.simat.trackingprovider/tracks"), null, null, null, null);
        boolean exists = cursor.moveToFirst();
        cursor.close();
        return exists;
    }

    public void setHHID(String HHID) {
        this.HHID = HHID;
    }

    public void setCompanyID(String companyID) {
        CompanyID = companyID;
    }

    public String getUUID() {
        TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        String UUID = tManager.getDeviceId();
        if (UUID == null)
            return Settings.Secure.getString(Contextor.getInstance().getContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        return tManager.getDeviceId();


    }

    public String getHHID() {
        InitData();
        return HHID;
    }

    public String getCompanyID() {
        InitData();
        return CompanyID;
    }

    public String getKEY_BASE_SOAP() {
        InitData();
        return KEY_BASE_SOAP + "/webservice.asmx?WSDL";
    }

    public void setKEY_BASE_SOAP(String KEY_BASE_SOAP) {
        this.KEY_BASE_SOAP = KEY_BASE_SOAP;
    }

    public String getKEY_BASE_SKYFROG() {
        InitData();
        return KEY_BASE_SKYFROG + "/store/";
    }

    public void setKEY_BASE_SKYFROG(String KEY_BASE_SKYFROG) {
        this.KEY_BASE_SKYFROG = KEY_BASE_SKYFROG;
    }

    public String getKEY_BASE_SKYFROG_MOBILE() {
        InitData();
       return KEY_BASE_SKYFROG_MOBILE + "/api/";
    }

    public void setKEY_BASE_SKYFROG_MOBILE(String KEY_BASE_SKYFROG_MOBILE) {
        this.KEY_BASE_SKYFROG_MOBILE = KEY_BASE_SKYFROG_MOBILE;
    }

    public String getKEY_BASE_INTERFACE() {
        InitData();
        return KEY_BASE_INTERFACE + "/POD/api/";
    }

    public void setKEY_BASE_INTERFACE(String KEY_BASE_INTERFACE) {
        this.KEY_BASE_INTERFACE = KEY_BASE_INTERFACE;
    }

    public String getKEY_BASE_NODE_API() {
        InitData();
        return KEY_BASE_NODE_API + "/api/";
    }

    public void setKEY_BASE_NODE_API(String KEY_BASE_NODE_API) {
        this.KEY_BASE_NODE_API = KEY_BASE_NODE_API;
    }

    public String getKEY_BASE_NODE() {
        InitData();
        return KEY_BASE_NODE;
    }

    public void setKEY_BASE_NODE(String KEY_BASE_NODE) {
        this.KEY_BASE_NODE = KEY_BASE_NODE;
    }

    public String getKEY_BASE_SOCKET() {
        InitData();
        return KEY_BASE_SOCKET;
    }

    public void setKEY_BASE_SOCKET(String KEY_BASE_SOCKET) {
        this.KEY_BASE_SOCKET = KEY_BASE_SOCKET;
    }

    public String getKEY_BASE_SOCKET_POST() {
        InitData();
        return KEY_BASE_SOCKET_POST;
    }

    public void setKEY_BASE_SOCKET_POST(String KEY_BASE_SOCKET_POST) {
        this.KEY_BASE_SOCKET_POST = KEY_BASE_SOCKET_POST;
    }

    public String getKEY_CHECK_EDIT_URL() {
        InitData();
        return KEY_CHECK_EDIT_URL;
    }

    public void setKEY_CHECK_EDIT_URL(String KEY_CHECK_EDIT_URL) {
        this.KEY_CHECK_EDIT_URL = KEY_CHECK_EDIT_URL;
    }

    public String getKEY_URL_FUEL() {
        InitData();
        return KEY_URL_FUEL;
    }

    public String getU_PodExit() {
        InitData();
        return U_PodExit;
    }

    public void setU_PodExit(String u_PodExit) {
        U_PodExit = u_PodExit;
    }

    public void setKEY_URL_FUEL(String KEY_URL_FUEL) {
        this.KEY_URL_FUEL = KEY_URL_FUEL;
    }

    public String getVersion() {

        String data = null;
        try {
            data = mContext.getPackageManager().getPackageInfo(
                    mContext.getPackageName(), 0).versionName;

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
        }
        return data;
    }

    public boolean IsLocalLanguage() {
        SharedPreferences pref = mContext.getSharedPreferences("SKYFROG",
                Context.MODE_PRIVATE);
        String lang = pref.getString("LANGUAGE", "English");
        return lang.equalsIgnoreCase("Local");

    }
}
