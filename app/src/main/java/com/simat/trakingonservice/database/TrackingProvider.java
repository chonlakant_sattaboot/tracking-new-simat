package com.simat.trakingonservice.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class TrackingProvider extends ContentProvider {

    public static final Uri TRACKING_CONTENT_URI = Uri.parse("content://com.simat.trackingprovider/tracks");
    public static final Uri MTRANS_CONTENT_URI = Uri.parse("content://com.simat.trackingprovider/mtrans");

    SkyFrogDatabaseHelper dbHelper;

    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbHelper = new SkyFrogDatabaseHelper(context, SkyFrogDatabaseHelper.DATABASE_NAME, null, SkyFrogDatabaseHelper.DATABASE_VERSION);

        return true;
    }

    //Create the constants used to differentiate between the different URI
    //requests.
    private static final int TRACKS = 1;
    private static final int MTRANS = 2;

    private static final UriMatcher uriMatcher;

    //Allocate the UriMatcher object, where a URI ending in 'earthquakes' will
    //correspond to a request for all earthquakes, and 'earthquakes' with a
    //trailing '/[rowID]' will represent a single earthquake row.
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("com.simat.trackingprovider", "tracks", TRACKS);
        uriMatcher.addURI("com.simat.trackingprovider", "mtrans", MTRANS);
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case TRACKS:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.simat.tracks";

            case MTRANS:
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.simat.mtrans";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sort) {

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        Cursor c = null;
        // If this is a row query, limit the result set to the passed in row.
        switch (uriMatcher.match(uri)) {
            case TRACKS:
                HashMap<String, String> SEARCH_PROJECTION_MAP = new HashMap<String, String>();
                SEARCH_PROJECTION_MAP.put("_id", "_id" + " as _id");
                SEARCH_PROJECTION_MAP.put("U_compID", "U_compID" + " as U_compID");
                SEARCH_PROJECTION_MAP.put("U_HHID", "U_HHID" + " as U_HHID");
                SEARCH_PROJECTION_MAP.put("U_success", "U_success" + " as U_success");
                SEARCH_PROJECTION_MAP.put("U_message", "U_message" + " as U_message");
                SEARCH_PROJECTION_MAP.put("U_lat", "U_lat" + " as U_lat");
                SEARCH_PROJECTION_MAP.put("U_lng", "U_lng" + " as U_lng");
                SEARCH_PROJECTION_MAP.put("U_dlat", "U_dlat" + " as U_dlat");
                SEARCH_PROJECTION_MAP.put("U_dlng", "U_dlng" + " as U_dlng");
                SEARCH_PROJECTION_MAP.put("U_radius", "U_radius" + " as U_radius");
                SEARCH_PROJECTION_MAP.put("U_battery", "U_battery" + " as U_battery");
                SEARCH_PROJECTION_MAP.put("U_distance", "U_distance" + " as U_distance");
                SEARCH_PROJECTION_MAP.put("U_poiID", "U_poiID" + " as U_poiID");
                SEARCH_PROJECTION_MAP.put("U_jobID", "U_jobID" + " as U_jobID");
                SEARCH_PROJECTION_MAP.put("U_checkInStatus", "U_checkInStatus" + " as U_checkInStatus");
                SEARCH_PROJECTION_MAP.put("U_StatusID", "U_StatusID" + " as U_StatusID");
                SEARCH_PROJECTION_MAP.put("U_StatusName", "U_StatusName" + " as U_StatusName");
                SEARCH_PROJECTION_MAP.put("U_NaviOpen", "U_NaviOpen" + " as U_NaviOpen");
                SEARCH_PROJECTION_MAP.put("U_NaviIsOpen", "U_NaviIsOpen" + " as U_NaviIsOpen");
                SEARCH_PROJECTION_MAP.put("U_DestinationName", "U_DestinationName" + " as U_DestinationName");
                SEARCH_PROJECTION_MAP.put("U_PodExit", "U_PodExit" + " as U_PodExit");
                SEARCH_PROJECTION_MAP.put("U_clat", "U_clat" + " as U_clat");
                SEARCH_PROJECTION_MAP.put("U_clng", "U_clng" + " as U_clng");
                SEARCH_PROJECTION_MAP.put("U_cjobID", "U_cjobID" + " as U_cjobID");
                SEARCH_PROJECTION_MAP.put("U_cjobStatus", "U_cjobStatus" + " as U_cjobStatus");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_SOAP", "KEY_BASE_SOAP" + " as KEY_BASE_SOAP");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_SKYFROG", "KEY_BASE_SKYFROG" + " as KEY_BASE_SKYFROG");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_SKYFROG_MOBILE", "KEY_BASE_SKYFROG_MOBILE" + " as KEY_BASE_SKYFROG_MOBILE");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_INTERFACE", "KEY_BASE_INTERFACE" + " as KEY_BASE_INTERFACE");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_NODE_API", "KEY_BASE_NODE_API" + " as KEY_BASE_NODE_API");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_NODE", "KEY_BASE_NODE" + " as KEY_BASE_NODE");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_SOCKET", "KEY_BASE_SOCKET" + " as KEY_BASE_SOCKET");
                SEARCH_PROJECTION_MAP.put("KEY_BASE_SOCKET_POST", "KEY_BASE_SOCKET_POST" + " as KEY_BASE_SOCKET_POST");
                SEARCH_PROJECTION_MAP.put("KEY_CHECK_EDIT_URL", "KEY_CHECK_EDIT_URL" + " as KEY_CHECK_EDIT_URL");
                SEARCH_PROJECTION_MAP.put("U_ajobStatus", "U_ajobStatus" + " as U_ajobStatus");

                qb.setStrict(true);
                qb.setProjectionMap(SEARCH_PROJECTION_MAP);
                qb.setTables(SkyFrogDatabaseHelper.TRACKING_TABLE);
                c = qb.query(database, projection, selection, selectionArgs, null, null, null);
                break;
            case MTRANS:

                HashMap<String, String> SEARCH_PROJECTION_MAP_MTRANS = new HashMap<String, String>();
                SEARCH_PROJECTION_MAP_MTRANS.put("_id", "_id" + " as _id");
                SEARCH_PROJECTION_MAP_MTRANS.put("hhid", "hhid" + " as hhid");
                SEARCH_PROJECTION_MAP_MTRANS.put("compid", "compid" + " as compid");
                SEARCH_PROJECTION_MAP_MTRANS.put("jobid", "jobid" + " as jobid");
                SEARCH_PROJECTION_MAP_MTRANS.put("latitude", "latitude" + " as latitude");
                SEARCH_PROJECTION_MAP_MTRANS.put("longitude", "longitude" + " as longitude");
                SEARCH_PROJECTION_MAP_MTRANS.put("speed", "speed" + " as speed");
                SEARCH_PROJECTION_MAP_MTRANS.put("maxspeedvalid", "maxspeedvalid" + " as maxspeedvalid");
                SEARCH_PROJECTION_MAP_MTRANS.put("minbatteryvalid", "minbatteryvalid" + " as minbatteryvalid");
                SEARCH_PROJECTION_MAP_MTRANS.put("currentconfigspeed", "currentconfigspeed" + " as currentconfigspeed");
                SEARCH_PROJECTION_MAP_MTRANS.put("currentconfigbattery", "currentconfigbattery" + " as currentconfigbattery");
                SEARCH_PROJECTION_MAP_MTRANS.put("geofenc", "geofenc" + " as geofenc");
                SEARCH_PROJECTION_MAP_MTRANS.put("direction", "direction" + " as direction");
                SEARCH_PROJECTION_MAP_MTRANS.put("battery", "battery" + " as battery");
                SEARCH_PROJECTION_MAP_MTRANS.put("gstatus", "gstatus" + " as gstatus");
                SEARCH_PROJECTION_MAP_MTRANS.put("createon", "createon" + " as createon");
                SEARCH_PROJECTION_MAP_MTRANS.put("distance", "distance" + " as distance");
                SEARCH_PROJECTION_MAP_MTRANS.put("cstatus", "cstatus" + " as cstatus");
                SEARCH_PROJECTION_MAP_MTRANS.put("dcstatus", "dcstatus" + " as dcstatus");
                SEARCH_PROJECTION_MAP_MTRANS.put("hhstatus", "hhstatus" + " as hhstatus");
                SEARCH_PROJECTION_MAP_MTRANS.put("actionapp", "actionapp" + " as actionapp");
                SEARCH_PROJECTION_MAP_MTRANS.put("accuracy", "accuracy" + " as accuracy");
                SEARCH_PROJECTION_MAP_MTRANS.put("ipaddress", "ipaddress" + " as ipaddress");
                SEARCH_PROJECTION_MAP_MTRANS.put("isstart", "isstart" + " as isstart");


                qb.setStrict(true);
                qb.setProjectionMap(SEARCH_PROJECTION_MAP_MTRANS);
                qb.setTables(MTransTable.TABLE_MTRANS);
                c = qb.query(database, projection, selection, selectionArgs, null, null, null);
                break;
            default:
                break;
        }


        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }


    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues _initialValues) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        switch (uriMatcher.match(uri)) {
            case TRACKS:
                // Insert the new row. The call to database.insert will return the row number
                // if it is successful.
                long trackingrowID = database.insert(SkyFrogDatabaseHelper.TRACKING_TABLE, "tracks", _initialValues);

                // Return a URI to the newly inserted row on success.
                if (trackingrowID > 0) {
                    Uri _uri = ContentUris.withAppendedId(TRACKING_CONTENT_URI, trackingrowID);
                    getContext().getContentResolver().notifyChange(_uri, null);
                    return uri;
                }
                break;

            case MTRANS:
                // Insert the new row. The call to database.insert will return the row number
                // if it is successful.
                long mtransrowID = database.insert(MTransTable.TABLE_MTRANS, "mtrans", _initialValues);

                // Return a URI to the newly inserted row on success.
                if (mtransrowID > 0) {
                    Uri _uri = ContentUris.withAppendedId(MTRANS_CONTENT_URI, mtransrowID);
                    getContext().getContentResolver().notifyChange(_uri, null);
                    return uri;
                }
                break;
        }
        throw new SQLException("Failed to insert row into " + uri);
    }

    @Nullable
    @Override
    public int delete(@NonNull Uri uri, String where, @Nullable String[] whereArgs) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        int count;
        switch (uriMatcher.match(uri)) {
            case TRACKS:


                count = database.delete(SkyFrogDatabaseHelper.TRACKING_TABLE, where, whereArgs);
                break;
            case MTRANS:


                count = database.delete(MTransTable.TABLE_MTRANS, where, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        int count;
        switch (uriMatcher.match(uri)) {
            case TRACKS:

                count = database.update(SkyFrogDatabaseHelper.TRACKING_TABLE, values, where, whereArgs);


                break;
            case MTRANS:
                count = database.update(MTransTable.TABLE_MTRANS, values, where, whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    //Helper class for opening, creating, and managing database version control
    private static class SkyFrogDatabaseHelper extends SQLiteOpenHelper {

        private static final String TAG = "TrackingProvider";
        private static final String DATABASE_NAME = "tracking.db";
        private static final int DATABASE_VERSION = 4;
        private static final String TRACKING_TABLE = "tracking_table";

        private static final String TRACKING_DATABASE_CREATE =
                "create table " + TRACKING_TABLE + " ("
                        + "_id" + " INTEGER PRIMARY KEY AUTOINCREMENT, "

                        + "U_compID" + " TEXT, "
                        + "U_HHID" + " TEXT, "
                        + "U_success" + " TEXT, "
                        + "U_message" + " TEXT, "
                        + "U_lat" + " TEXT, "
                        + "U_lng" + " TEXT, "

                        + "U_dlat" + " TEXT, "
                        + "U_dlng" + " TEXT, "

                        + "U_radius" + " TEXT, "
                        + "U_battery" + " TEXT, "
                        + "U_distance" + " REAL, "
                        + "U_poiID" + " TEXT, "

                        + "U_jobID" + " TEXT, "
                        + "U_checkInStatus" + " TEXT, "
                        + "U_StatusID" + " TEXT, "
                        + "U_StatusName" + " TEXT, "

                        + "U_NaviOpen" + " TEXT, "
                        + "U_NaviIsOpen" + " TEXT, "
                        + "U_DestinationName" + " TEXT, "
                        + "U_PodExit" + " TEXT, "

                        + "U_clat" + " TEXT, "
                        + "U_clng" + " TEXT, "
                        + "U_cjobID" + " TEXT, "
                        + "U_cjobStatus" + " TEXT, "
                        + "KEY_BASE_SOAP" + " TEXT, "
                        + "KEY_BASE_SKYFROG" + " TEXT, "
                        + "KEY_BASE_SKYFROG_MOBILE" + " TEXT, "
                        + "KEY_BASE_INTERFACE" + " TEXT, "
                        + "KEY_BASE_NODE_API" + " TEXT, "
                        + "KEY_BASE_NODE" + " TEXT, "
                        + "KEY_BASE_SOCKET" + " TEXT, "
                        + "KEY_BASE_SOCKET_POST" + " TEXT, "
                        + "KEY_CHECK_EDIT_URL" + " TEXT, "
                        + "U_ajobStatus" + " TEXT);";


        public SkyFrogDatabaseHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TRACKING_DATABASE_CREATE);
            db.execSQL(MTransTable.DATABASE_CREATE);

            ContentValues values = new ContentValues();
            values.put("U_compID", "0");
            values.put("U_HHID", "0");
            values.put("U_success", "N");
            values.put("U_message", "N");
            values.put("U_lat", "0.0");
            values.put("U_lng", "0.0");

            values.put("U_dlat", "0.0");
            values.put("U_dlng", "0.0");

            values.put("U_radius", "100");
            values.put("U_battery", "100");
            values.put("U_distance", "0.0");
            values.put("U_poiID", "100");

            values.put("U_jobID", "0");
            values.put("U_checkInStatus", "N");
            values.put("U_StatusID", "");
            values.put("U_StatusName", "");
            values.put("U_PodExit", "");

            values.put("U_NaviOpen", "N");
            values.put("U_NaviIsOpen", "N");
            values.put("U_DestinationName", "N");


            values.put("U_clat", "0.0");
            values.put("U_clng", "0.0");
            values.put("U_cjobID", "0");
            values.put("U_cjobStatus", "0");
            values.put("KEY_BASE_SOAP", "http://soap.skyfrog.net");
            values.put("KEY_BASE_SKYFROG", "https://skyfrog.net");
            values.put("KEY_BASE_SKYFROG_MOBILE", "http://mobile.skyfrog.net");
            values.put("KEY_BASE_INTERFACE", "https://interface.skyfrog.net");
            values.put("KEY_BASE_NODE_API", "https://nodeapi.skyfrog.net");
            values.put("KEY_BASE_NODE", "https://node.skyfrog.net");
            values.put("KEY_BASE_SOCKET", "socket.skyfrog.net");
            values.put("KEY_BASE_SOCKET_POST", "1301");
            values.put("KEY_CHECK_EDIT_URL", "");
            values.put("U_ajobStatus", "0");

            db.insert(TRACKING_TABLE, "dummy", values);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");

            Log.e("Check_data_base", "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");

            for (int i = oldVersion + 1; i <= newVersion; i++) {
                try {
                    switch (i) {
                        case 2:
                            try {
                                //Update Tracking Table
                                String sql = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "U_PodExit" + " TEXT";
                                db.execSQL(sql);
                                db.execSQL(MTransTable.DATABASE_CREATE);
                                AddLogcat("OnUpgrade", "Update v " + i + "Complete");
                            } catch (Exception e) {
                                AddLogcat("OnUpgrade", e.toString());
                            }
                            break;

                        case 4:
                            try {
                                //Update Tracking Table
                                String sql1 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_SOAP" + " TEXT";
                                String sql2 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_SKYFROG" + " TEXT";
                                String sql3 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_SKYFROG_MOBILE" + " TEXT";
                                String sql4 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_INTERFACE" + " TEXT";
                                String sql5 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_NODE_API" + " TEXT";
                                String sql6 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_NODE" + " TEXT";
                                String sql7 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_SOCKET" + " TEXT";
                                String sql8 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_BASE_SOCKET_POST" + " TEXT";
                                String sql9 = "ALTER TABLE " + TRACKING_TABLE + " ADD COLUMN " + "KEY_CHECK_EDIT_URL" + " TEXT";

                                db.execSQL(sql1);
                                db.execSQL(sql2);
                                db.execSQL(sql3);
                                db.execSQL(sql4);
                                db.execSQL(sql5);
                                db.execSQL(sql6);
                                db.execSQL(sql7);
                                db.execSQL(sql8);
                                db.execSQL(sql9);

                                db.execSQL(MTransTable.DATABASE_CREATE);
                                AddLogcat("OnUpgrade", "Update v " + i + "Complete");
                            } catch (Exception e) {
                                AddLogcat("OnUpgrade", e.toString());
                            }
                            break;


                    }


                } catch (Exception e) {
                    AddLogcat("OnUpgrade", e.toString());
                }
            }

        }

        private void AddLogcat(String title, String detail) {
            try {
                String namefile = "logdb.txt";
                String namepathh = "/Android/data/com.simat.trakingonservice/LOG";

                File path = new File(Environment.getExternalStorageDirectory() + namepathh);
                File file = new File(path, namefile);
                if (!path.isDirectory()) {
                    path.mkdirs();
                }
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    //BufferedWriter for performance, true to set append to file flag
                    BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
                    buf.append(title + "->");
                    buf.append(detail);
                    buf.newLine();
                    buf.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}