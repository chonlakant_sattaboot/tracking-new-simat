package com.simat.trakingonservice.database;

public class MTransTable {
	// Database table
	public static final String TABLE_MTRANS = "mtrans_table";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_HHID = "hhid";
	public static final String COLUMN_COMPID = "compid";
	public static final String COLUMN_JOBID = "jobid";
	public static final String COLUMN_LATI = "latitude";
	public static final String COLUMN_LONG = "longitude";
	public static final String COLUMN_Speed = "speed";
	public static final String COLUMN_MaxSpeedValid = "maxspeedvalid";
	public static final String COLUMN_MinBatteryValid = "minbatteryvalid";
	public static final String COLUMN_CurrentConfigSpeed = "currentconfigspeed";
	public static final String COLUMN_CurrentConfigBattery = "currentconfigbattery";
	public static final String COLUMN_GEOFENC = "geofenc";
	public static final String COLUMN_Direction = "direction";
	public static final String COLUMN_Battery = "battery";
	public static final String COLUMN_GStatus = "gstatus";
	public static final String COLUMN_CreateOn = "createon";
	public static final String COLUMN_Distance = "distance";
	public static final String COLUMN_CStatus = "cstatus";
	public static final String COLUMN_DCStatus = "dcstatus";
	public static final String COLUMN_hhstatus = "hhstatus";
	public static final String COLUMN_ACTIONAPP = "actionapp";
	public static final String COLUMN_Accuracy = "accuracy";
	public static final String COLUMN_IPADDRESS = "ipaddress";
	public static final String COLUMN_IsStart = "isstart";

	// Database creation SQL statement
	public static final String DATABASE_CREATE = "create table " + TABLE_MTRANS
			+ " (" + COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_HHID + " text , "
			+ COLUMN_COMPID + " text , "
			+ COLUMN_JOBID + " text , "
			+ COLUMN_LATI + " text , "
			+ COLUMN_LONG + " text , "
			+ COLUMN_Speed + " text , "
			+ COLUMN_MaxSpeedValid + " text , "
			+ COLUMN_MinBatteryValid + " text , "
			+ COLUMN_CurrentConfigSpeed + " text , "
			+ COLUMN_CurrentConfigBattery + " text , "
			+ COLUMN_GEOFENC + " text , "
			+ COLUMN_Direction + " text , "
			+ COLUMN_Battery + " text , "
			+ COLUMN_GStatus + " text , "
			+ COLUMN_CreateOn + " text , "
			+ COLUMN_Distance + " text , "
			+ COLUMN_CStatus + " text , "
			+ COLUMN_DCStatus + " text , "
			+ COLUMN_hhstatus + " text , "
			+ COLUMN_ACTIONAPP + " text , "
			+ COLUMN_Accuracy + " text , "
			+ COLUMN_IPADDRESS + " text , "
			+ COLUMN_IsStart + " text " + ");";

}
