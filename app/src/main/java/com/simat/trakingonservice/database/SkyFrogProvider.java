package com.simat.trakingonservice.database;

import android.net.Uri;

public class SkyFrogProvider {

	public static final Uri JOBH_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/jobhs");

	public static final Uri JOBD_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/jobds");

	public static final Uri RES_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/res");

	public static final Uri QAS_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/qas");

	public static final Uri EXP_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/exp");

	public static final Uri EXPS_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/exps");

	public static final Uri SETTING_JOB_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/settingjobs");

	public static final Uri SETTING_ITEM_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/settingitems");

	public static final Uri HST_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/hst");

	public static final Uri HCJ_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/hcj");

	public static final Uri HCI_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/hci");

	public static final Uri LANGUAGE_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/language");

	public static final Uri PREF_CONTENT_URI = Uri
			.parse("content://com.simat.skyfrogprovider/pref");
}