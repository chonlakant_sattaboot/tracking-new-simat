package com.simat.trakingonservice.event;

import com.squareup.otto.Bus;


public class TimeBus
{
    private static Bus instance = null;

    private TimeBus()
    {
        instance = new Bus();
    }

    public static Bus getInstance()
    {
        if(instance == null)
        {
            instance = new Bus();
        }
        return instance;
    }
}