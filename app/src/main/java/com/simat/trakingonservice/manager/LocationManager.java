package com.simat.trakingonservice.manager;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.simat.trakingonservice.location.BestLocationListener;
import com.simat.trakingonservice.location.LocationDao;


/**
 * Created by NileZia on 6/13/2016.
 */
public class LocationManager implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private GoogleApiClient googleApiClient;
    private Context mContext;
    private LocationRequest locationRequest;
    private BestLocationListener bestLocationListener;
    private LocationAvailability locationAvailability;


    private boolean isLocationUpdateStart = false;

    public LocationManager(Context context) {
        this.mContext = context;
    }

    public void build() {

        googleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        connect();

    }

    public void connect() {
        googleApiClient.connect();
    }


    public LocationManager setBestLocationListener(BestLocationListener bestLocationListener) {
        this.bestLocationListener = bestLocationListener;
        return this;
    }

    public LocationManager setLocationRequest(LocationRequest locationRequest) {
        this.locationRequest = locationRequest;
        return this;
    }

    public boolean isLocationAvailable() {
        locationAvailability = LocationServices.FusedLocationApi.getLocationAvailability(googleApiClient);
        return locationAvailability.isLocationAvailable();
    }

    public boolean isGoogleApiClientConnected() {
        return googleApiClient != null && googleApiClient.isConnected();
    }

    public boolean isGoogleApiClientConnecting() {
        return googleApiClient != null && googleApiClient.isConnecting();
    }


    public void reconnect() {
        if (googleApiClient != null)
            googleApiClient.reconnect();
    }

    public void disconnect() {
        if (googleApiClient != null)
            googleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        locationAvailability = LocationServices.FusedLocationApi.getLocationAvailability(googleApiClient);
        if (locationAvailability != null) {
            if (locationAvailability.isLocationAvailable()) {
                startLocationUpdate();
            }
        }
    }

    public void startLocationUpdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.reconnect();
        Log.d("checklocation", "onConnectionSuspended : reconnect " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        isLocationUpdateStart = false;

    }

    @Override
    public void onLocationChanged(final Location location) {
        isLocationUpdateStart = true;
        LocationDao.getInstance().setLocation(location);
        if (bestLocationListener != null) {
            bestLocationListener.onLocationUpdate(location, true);


        }
    }

    public boolean isLocationUpdateStart() {
        return isLocationUpdateStart;
    }

    public void setLocationUpdateStart(boolean locationUpdateStart) {
        isLocationUpdateStart = locationUpdateStart;
    }

}
