package com.simat.trakingonservice.manager;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;


public class TaskManager {

    public TaskManager() {

    }

    public static boolean isMapRunningInForeground() {

        ActivityManager mActivityManager = (ActivityManager) Contextor.getInstance()
                .getContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT > 20) {
            String mPackageName = mActivityManager.getRunningAppProcesses().get(0).processName;
            return mPackageName.equalsIgnoreCase("com.google.android.apps.maps");
        } else {
            String mpackageName = mActivityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
            return mpackageName.equalsIgnoreCase("com.google.android.apps.maps");
        }
    }

    public static boolean isTrackingRunningInForeground() {
        ActivityManager mActivityManager = (ActivityManager) Contextor.getInstance()
                .getContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > 20) {
            String mPackageName = mActivityManager.getRunningAppProcesses().get(0).processName;
            return mPackageName.equalsIgnoreCase("com.simat.trakingonservice");
        } else {
            String mpackageName = mActivityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
            return mpackageName.equalsIgnoreCase("com.simat.trakingonservice");

        }
    }

    public static boolean isPODRunningInForeground() {

        ActivityManager mActivityManager = (ActivityManager) Contextor.getInstance()
                .getContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT > 20) {
            String mPackageName = mActivityManager.getRunningAppProcesses().get(0).processName;
            return mPackageName.equalsIgnoreCase("com.simat");
        } else {
            String mpackageName = mActivityManager.getRunningTasks(1).get(0).topActivity.getPackageName();
            return mpackageName.equalsIgnoreCase("com.simat");

        }

    }
}
