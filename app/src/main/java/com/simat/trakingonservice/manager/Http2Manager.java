package com.simat.trakingonservice.manager;

import android.content.Context;

import com.simat.trakingonservice.manager.http.ApiService;
import com.simat.trakingonservice.model.CTranModel;
import com.simat.trakingonservice.util.DeviceUrl;
import com.simat.trakingonservice.util.Utility;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Http2Manager {

    // private static Http2Manager instance;
    private ApiService service;


    public Http2Manager() {
        Context mContext = Contextor.getInstance().getContext();
        CTranModel cTranModel = new CTranModel(mContext);
        final String HHID = cTranModel.getTrackingModel().getU_HHID();
        final String CompanyID = cTranModel.getTrackingModel().getU_compID();
        final String UUID = new Utility().getUUID();
        try {

            Interceptor interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    String credential = Credentials.basic("sim@ts0ft", "dArgYwXdp69WqbgA");
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", credential)
                            .addHeader("x-access-hhid", HHID)
                            .addHeader("x-access-company", CompanyID)
                            .addHeader("x-access-uuid", UUID)
                            .build();


                    return chain.proceed(newRequest);
                }
            };

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.interceptors().add(interceptor);

            OkHttpClient client = builder
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(DeviceUrl.getInstance().getKEY_BASE_NODE_API())
                    .addConverterFactory(GsonConverterFactory.create()).client(client)
                    .client(getUnsafeOkHttpClient())
                    .build();

            service = retrofit.create(ApiService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public ApiService getService() {
        return service;
    }


    public static OkHttpClient getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}