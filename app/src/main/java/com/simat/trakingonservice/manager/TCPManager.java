package com.simat.trakingonservice.manager;

import android.content.Context;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;


/**
 * Created by NileZia on 7/20/2016.
 */
public class TCPManager {
    private static TCPManager instance;
    private Socket socket;
    private LocationManager locationManager;

    private TCPManager() {

    }

    public static TCPManager getInstance() {
        if (instance == null)
            instance = new TCPManager();
        return instance;
    }

    public Socket getSocket() {
        return socket;
    }

    public void send(String url, int post, String message, SendCallback callback) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            new Client(url, post, message, callback).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
        else
            new Client(url, post, message, callback).execute((Void[]) null);

    }

    private class Client extends AsyncTask<Void, Void, Void> {

        String response = "";
        String message = "";
        SendCallback callback;
        String url = "";
        int post;


        Client(String url, int port, String message, SendCallback callback) {
            this.callback = callback;
            this.message = message;
            this.url = url;
            this.post = port;
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {

                InetAddress serverAddr = InetAddress.getByName(url);
                socket = new Socket(serverAddr, post);
                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(getSocket().getOutputStream())), true);

                out.println(message);
                out.flush();

                if (callback != null) {

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            callback.onSuccess("Success");

                            Log.e("message", message.toString() + "ddd");
                            Log.e("IP_ADDRESS", url + ":" + post);


                        }
                    });
                }

            } catch (Exception e) {

                response = e.toString();
                onError();

            }

            return null;
        }

        private void onError() {
            if (callback != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        callback.onFailed(response);

                    }
                });
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }

    }

    public interface SendCallback {
        void onSuccess(String message);

        void onFailed(String message);
    }


    public void stopSocket(String url, int post) {
        try {
            InetAddress serverAddr = InetAddress.getByName(url);
            socket = new Socket(serverAddr, post);
            socket.isClosed();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isLocationEnabled() {
        String le = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) Contextor.getInstance().getContext().getSystemService(le);
        if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }


}