package com.simat.trakingonservice.manager;

import android.content.Context;
import android.util.Log;

import com.simat.trakingonservice.manager.http.ApiService;
import com.simat.trakingonservice.model.CTranModel;
import com.simat.trakingonservice.util.Utility;
import com.simat.trakingonservice.util.constanstURL;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpConfigManager {
    private ApiService service;

    public HttpConfigManager() {
        Context mContext = Contextor.getInstance().getContext();

        try {

//            Interceptor interceptor = new Interceptor() {
//                @Override
//                public okhttp3.Response intercept(Chain chain) throws IOException {
//                    String credential = Credentials.basic("sim@ts0ft", "dArgYwXdp69WqbgA");
//                    // Log.e("credential",credential);
//                    Request newRequest = chain.request().newBuilder()
//                            .addHeader("Authorization", "Basic U2ltQHRTMGZ0OlNreUZyMGdAUzBmdA==")
//                            .addHeader("Content-Type", "application/json")
//                            .addHeader("CompanyID", company)
//                            .build();
//
//                    return chain.proceed(newRequest);
//                }
//            };

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            // builder.interceptors().add(interceptor);

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);


            OkHttpClient client = builder
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(constanstURL.baseUrlConfig)
                    .addConverterFactory(GsonConverterFactory.create()).client(client)
                    .build();


            service = retrofit.create(ApiService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public ApiService getService() {
        return service;
    }

}