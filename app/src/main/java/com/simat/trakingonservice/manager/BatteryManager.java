package com.simat.trakingonservice.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.simat.trakingonservice.controller.MainApplication;
import com.simat.trakingonservice.location.LocationDao;
import com.simat.trakingonservice.model.CTranModel;
import com.simat.trakingonservice.util.ConfigInformation;
import com.simat.trakingonservice.util.DateFormatUtil;
import com.simat.trakingonservice.util.DeviceUrl;
import com.simat.trakingonservice.util.Utility;

public class BatteryManager {

    private final Context mContext;
    static boolean isBatteryRunning = false;
    private int BatteryLimit;
    private int BatteryLevel;
    private int soundCounter;

    private int BatteryAlert;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private MainApplication trackingController;

    private boolean isPluge;

    @SuppressLint("CommitPrefEdits")
    public BatteryManager() {

        this.mContext = Contextor.getInstance().getContext();
        this.trackingController = (MainApplication) mContext;
        preferences = mContext.getSharedPreferences(ConfigInformation.PREF_CONFIG, Context.MODE_PRIVATE);
        editor = preferences.edit();


    }

    public void checkBattery() {


        boolean isSuccess = Boolean.valueOf(new CTranModel(mContext).getTrackingModel().getU_success());
        isBatteryRunning = true;

        if (isSuccess) {

            if ((!isPluge) && (BatteryLimit > BatteryLevel) && (BatteryLevel >= 0)) {
                Log.d("BatteryCheck", "BatteryLimit : " + BatteryLimit + " BatteryLevel : " + BatteryLevel);


                if (BatteryAlert == 0) {
                    if (!TaskManager.isMapRunningInForeground())
                        soundCounter = 10;

                    BatteryAlert = 1;

                    Log.d("BatteryCheck", "Send >>>> " + "BatteryLimit : " + BatteryLimit + " BatteryLevel : " + BatteryLevel);

                    Log.e("BatteryCheck", DeviceUrl.getInstance().getKEY_BASE_SOCKET()+" : "+
                            Integer.parseInt(DeviceUrl.getInstance().getKEY_BASE_SOCKET_POST()));

                    String createDate = DateFormatUtil.parseDateToyyMMddkkmmss(trackingController.getMyService().getTimeSync());
                    String dataSend = "*SM," + new CTranModel(mContext).getTrackingModel().getU_compID() + ","
                            + new CTranModel(mContext).getTrackingModel().getU_HHID() + ","
                            + new Utility().getDeviceId() + ","
                            + "A101" + ","
                            + "1001" + ","
                            + createDate + ","
                            + LocationDao.getInstance().getLocation().getLatitude() + "," +
                            +LocationDao.getInstance().getLocation().getLongitude() + "," +
                            +BatteryLimit + "," +
                            +BatteryLevel + ",##";

                    TCPManager.getInstance().send(DeviceUrl.getInstance().getKEY_BASE_SOCKET(),
                            Integer.parseInt(DeviceUrl.getInstance().getKEY_BASE_SOCKET_POST()),dataSend, new TCPManager.SendCallback() {
                        @Override
                        public void onSuccess(String message) {
                            Log.i("Websocket", "onSuccess >>>  " + "Battery");
                        }

                        @Override
                        public void onFailed(String message) {
                            Log.i("Websocket", "onFailed >>>  " + "Battery");
                        }
                    });
                }
            } else {

                BatteryAlert = 0;
                soundCounter = 0;


            }
        }

    }



    public int getBatteryLimit() {

        this.BatteryLimit = preferences.getInt("batteryLimit", 30);
        return BatteryLimit;
    }

    public void setBatteryLimit(int batteryLimit) {
        this.BatteryLimit = batteryLimit;
        editor.putInt("batteryLimit", BatteryLimit);
        apply();
    }

    public int getBatteryLevel() {
        BatteryLevel = preferences.getInt("batteryLevel", 50);
        return BatteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.BatteryLevel = batteryLevel;
        editor.putInt("batteryLevel", batteryLevel);
        apply();
    }

    public void apply() {
        editor.apply();
    }

    public int getSoundCounter() {
        return soundCounter;
    }

    public void setSoundCounter(int soundCounter) {
        this.soundCounter = soundCounter;
    }

    public boolean isPluge() {
        return isPluge;
    }

    public void setPluge(boolean pluge) {
        isPluge = pluge;
    }
}
