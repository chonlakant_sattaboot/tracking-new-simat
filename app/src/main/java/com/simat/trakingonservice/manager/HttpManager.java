package com.simat.trakingonservice.manager;

import android.content.Context;
import android.util.Log;


import com.simat.trakingonservice.manager.http.ApiService;
import com.simat.trakingonservice.model.CTranModel;
import com.simat.trakingonservice.util.DeviceUrl;
import com.simat.trakingonservice.util.GetConfigPre;
import com.simat.trakingonservice.util.Utility;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpManager {

    private static HttpManager instance;
    private ApiService service;

    public static HttpManager getInstance() {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }

    private Context mContext;

    private HttpManager() {
        mContext = Contextor.getInstance().getContext();
        CTranModel cTranModel = new CTranModel(mContext);
        final String HHID = cTranModel.getTrackingModel().getU_HHID();
        final String CompanyID = cTranModel.getTrackingModel().getU_compID();
        final String UUID = new Utility().getUUID();
        final String DeviceID = new Utility().getDeviceId();
        try {
            Interceptor interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    String credential = Credentials.basic("skyfrog", "p@ssw0rd");
                    Request newRequest = chain.request().newBuilder()
                            .addHeader("Authorization", credential)
                            .addHeader("HHID", HHID)
                            .addHeader("CompanyID", CompanyID)
                            .addHeader("UUID", UUID)
                            .addHeader("UDID", DeviceID)
                            .build();

                    return chain.proceed(newRequest);
                }
            };


// Add the interceptor to OkHttpClient
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            //  builder.sslSocketFactory(getSSLSocketFactory());
            builder.interceptors().add(interceptor);
            OkHttpClient client = builder.build();




            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(DeviceUrl.getInstance().getKEY_BASE_SKYFROG_MOBILE())
                    .addConverterFactory(GsonConverterFactory.create()).client(client)
                    .client(client)
                    .build();

            Log.e("BBBB", DeviceUrl.getInstance().getKEY_BASE_SKYFROG_MOBILE() + ": + " + GetConfigPre.getInstance().getBaseSkyfrogMobile());

            service = retrofit.create(ApiService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ApiService getService() {
        return service;
    }

    public void Reset() {
        instance = null;
    }


}