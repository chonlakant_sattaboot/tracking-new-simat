package com.simat.trakingonservice.manager.http;

import com.simat.trakingonservice.model.ActivationModel;
import com.simat.trakingonservice.model.ConfigModel;
import com.simat.trakingonservice.model.DeviceStatus;
import com.simat.trakingonservice.model.MTranModel;
import com.simat.trakingonservice.model.OfflineFileData;
import com.simat.trakingonservice.model.ProfileModel;
import com.simat.trakingonservice.model.ResultConfigUrl;
import com.simat.trakingonservice.model.ResultResponse;
import com.simat.trakingonservice.model.ResultWithModel;
import com.simat.trakingonservice.model.SmartPhone;
import com.simat.trakingonservice.model.UpdateVersion;
import com.simat.trakingonservice.model.VersionModel;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by NileZia on 25/3/2559.
 */
public interface ApiService {

    @GET("Tracking/Config")
    Call<ConfigModel> Config(@Query("refCode") String refCode);

    @GET("Tracking/DeviceStatus")
    Call<DeviceStatus> DeviceStatus();


    @POST("Tracking/UpdateVersion")
    Call<UpdateVersion> UpdateVersion(@Body VersionModel version);

    //Activation
    @POST("Tracking/Activation")
    Call<ProfileModel> Activation(@Body ActivationModel activationModel);


    //GTE URL
    @GET("Systems/ServiceUrl2/{CompanyCode}")
    Call<ResultConfigUrl> GetConfigUrl(@Path("CompanyCode") String CompanyCode);

    @POST("Tracking/TrackingOffline")
    Call<ResultWithModel> TrackingOffline(@Body List<MTranModel> MTranModel);

    @Multipart
    @POST("Tracking/UploadFile")
    Call<OfflineFileData> UploadFile(@Part("description") RequestBody description,
                                     @Part MultipartBody.Part file);


    @POST("logger/smartphone")
    Call<ResultResponse> SmartPhone(@Body List<SmartPhone> smartPhones);

}
