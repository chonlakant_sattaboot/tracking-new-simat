package com.simat.trakingonservice.location;


import android.location.Location;

public abstract class BestLocationListener {

	public abstract void onLocationUpdate(Location location, boolean isFresh);

}
