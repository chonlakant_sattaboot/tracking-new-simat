package com.simat.trakingonservice.location;

import android.location.Location;

/**
 * Created by nuuneoi on 12/6/14 AD.
 */
public class LocationDao {

    private static LocationDao instance;
    private Location location;
    private double speed;

    public static LocationDao getInstance() {
        if (instance == null)
            instance = new LocationDao();
        return instance;
    }

    private LocationDao() {

    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
